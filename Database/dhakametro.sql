-- phpMyAdmin SQL Dump
-- version 5.3.0-dev+20221025.2f3aafed4c
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 08, 2023 at 12:17 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dhakametro`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` bigint(20) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can view log entry', 1, 'view_logentry'),
(5, 'Can add permission', 2, 'add_permission'),
(6, 'Can change permission', 2, 'change_permission'),
(7, 'Can delete permission', 2, 'delete_permission'),
(8, 'Can view permission', 2, 'view_permission'),
(9, 'Can add group', 3, 'add_group'),
(10, 'Can change group', 3, 'change_group'),
(11, 'Can delete group', 3, 'delete_group'),
(12, 'Can view group', 3, 'view_group'),
(13, 'Can add content type', 4, 'add_contenttype'),
(14, 'Can change content type', 4, 'change_contenttype'),
(15, 'Can delete content type', 4, 'delete_contenttype'),
(16, 'Can view content type', 4, 'view_contenttype'),
(17, 'Can add session', 5, 'add_session'),
(18, 'Can change session', 5, 'change_session'),
(19, 'Can delete session', 5, 'delete_session'),
(20, 'Can view session', 5, 'view_session'),
(21, 'Can add user', 6, 'add_user'),
(22, 'Can change user', 6, 'change_user'),
(23, 'Can delete user', 6, 'delete_user'),
(24, 'Can view user', 6, 'view_user'),
(25, 'Can add address', 7, 'add_address'),
(26, 'Can change address', 7, 'change_address'),
(27, 'Can delete address', 7, 'delete_address'),
(28, 'Can view address', 7, 'view_address'),
(29, 'Can add brand', 8, 'add_brand'),
(30, 'Can change brand', 8, 'change_brand'),
(31, 'Can delete brand', 8, 'delete_brand'),
(32, 'Can view brand', 8, 'view_brand'),
(33, 'Can add category', 9, 'add_category'),
(34, 'Can change category', 9, 'change_category'),
(35, 'Can delete category', 9, 'delete_category'),
(36, 'Can view category', 9, 'view_category'),
(37, 'Can add product', 10, 'add_product'),
(38, 'Can change product', 10, 'change_product'),
(39, 'Can delete product', 10, 'delete_product'),
(40, 'Can view product', 10, 'view_product'),
(41, 'Can add product tag', 11, 'add_producttag'),
(42, 'Can change product tag', 11, 'change_producttag'),
(43, 'Can delete product tag', 11, 'delete_producttag'),
(44, 'Can view product tag', 11, 'view_producttag'),
(45, 'Can add product variation', 12, 'add_productvariation'),
(46, 'Can change product variation', 12, 'change_productvariation'),
(47, 'Can delete product variation', 12, 'delete_productvariation'),
(48, 'Can view product variation', 12, 'view_productvariation'),
(49, 'Can add subcategory', 13, 'add_subcategory'),
(50, 'Can change subcategory', 13, 'change_subcategory'),
(51, 'Can delete subcategory', 13, 'delete_subcategory'),
(52, 'Can view subcategory', 13, 'view_subcategory'),
(53, 'Can add product reward', 14, 'add_productreward'),
(54, 'Can change product reward', 14, 'change_productreward'),
(55, 'Can delete product reward', 14, 'delete_productreward'),
(56, 'Can view product reward', 14, 'view_productreward'),
(57, 'Can add product image', 15, 'add_productimage'),
(58, 'Can change product image', 15, 'change_productimage'),
(59, 'Can delete product image', 15, 'delete_productimage'),
(60, 'Can view product image', 15, 'view_productimage'),
(61, 'Can add order', 16, 'add_order'),
(62, 'Can change order', 16, 'change_order'),
(63, 'Can delete order', 16, 'delete_order'),
(64, 'Can view order', 16, 'view_order'),
(65, 'Can add cart', 17, 'add_cart'),
(66, 'Can change cart', 17, 'change_cart'),
(67, 'Can delete cart', 17, 'delete_cart'),
(68, 'Can view cart', 17, 'view_cart');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_address`
--

CREATE TABLE `auth_user_address` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `street_address` varchar(255) NOT NULL,
  `zip_code` varchar(4) DEFAULT NULL,
  `city` varchar(255) NOT NULL,
  `billing_address` tinyint(1) NOT NULL,
  `current_select` tinyint(1) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `user_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `auth_user_address`
--

INSERT INTO `auth_user_address` (`id`, `name`, `phone`, `street_address`, `zip_code`, `city`, `billing_address`, `current_select`, `created_at`, `updated_at`, `user_id`) VALUES
(1, 'Apon Saha', '01702081812', 'North South University(gate number-3) Plot-15, Block-B, Aftabuddin Road, Bashundhara, Dhaka-1229, Bangladesh', '1229', 'Dhaka', 1, 0, '2023-01-02 08:53:14.413103', '2023-01-02 11:12:58.774173', 1),
(2, 'Anik saha', '01799999888', 'Aftabuddin Road, Bashundhara', '1222', 'Dhaka', 0, 1, '2023-01-02 11:12:47.148202', '2023-01-08 09:58:59.893110', 1);

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user`
--

CREATE TABLE `auth_user_user` (
  `id` bigint(20) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `email` varchar(254) NOT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `auth_user_user`
--

INSERT INTO `auth_user_user` (`id`, `password`, `last_login`, `is_superuser`, `is_staff`, `is_active`, `date_joined`, `full_name`, `email`, `phone`, `image`) VALUES
(1, 'pbkdf2_sha256$390000$gc6RcvUyPaho2XrOHb7ILQ$ILKff/b8uGsH+PCtphqV3CNeahB/Br/Wi5/RAdgZg7A=', '2023-01-05 08:11:25.747172', 1, 1, 1, '2022-12-20 11:10:08.000000', 'admin', 'admin@gmail.com', '01788888888', '');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_groups`
--

CREATE TABLE `auth_user_user_groups` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_user_permissions`
--

CREATE TABLE `auth_user_user_user_permissions` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext DEFAULT NULL,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL CHECK (`action_flag` >= 0),
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `django_admin_log`
--

INSERT INTO `django_admin_log` (`id`, `action_time`, `object_id`, `object_repr`, `action_flag`, `change_message`, `content_type_id`, `user_id`) VALUES
(1, '2022-12-20 11:20:03.019837', '1', 'FREE RACE VISOR', 1, '[{\"added\": {}}]', 10, 1),
(2, '2022-12-20 11:20:20.429605', '2', '35% OFF', 1, '[{\"added\": {}}]', 10, 1),
(3, '2022-12-20 11:20:26.127710', '3', '20% OFF', 1, '[{\"added\": {}}]', 10, 1),
(4, '2022-12-20 11:20:32.309428', '4', '25% OFF', 1, '[{\"added\": {}}]', 10, 1),
(5, '2022-12-20 11:20:43.133652', '5', 'OVER 10% OFF', 1, '[{\"added\": {}}]', 10, 1),
(6, '2022-12-20 11:20:48.243799', '6', 'OVER 20% OFF', 1, '[{\"added\": {}}]', 10, 1),
(7, '2022-12-20 11:20:55.196225', '7', 'OVER 25% OFF', 1, '[{\"added\": {}}]', 10, 1),
(8, '2022-12-20 11:21:01.401428', '8', 'OVER 35% OFF', 1, '[{\"added\": {}}]', 10, 1),
(9, '2022-12-20 11:21:09.550453', '9', 'BEST BUY', 1, '[{\"added\": {}}]', 10, 1),
(10, '2022-12-20 11:23:14.465378', '1', 'S : 55 - 56', 1, '[{\"added\": {}}]', 11, 1),
(11, '2022-12-20 11:23:23.782376', '2', 'M : 57 - 58', 1, '[{\"added\": {}}]', 11, 1),
(12, '2022-12-20 11:23:30.300113', '3', 'XL : 61 - 62', 1, '[{\"added\": {}}]', 11, 1),
(13, '2022-12-20 11:23:38.357299', '4', 'L : 59 - 60', 1, '[{\"added\": {}}]', 11, 1),
(14, '2022-12-20 11:28:00.557058', '1', 'Shark S900 Dual Special Edition - Matt Black', 1, '[{\"added\": {}}, {\"added\": {\"name\": \"product image\", \"object\": \"Shark S900 Dual Special Edition - Matt Black\"}}, {\"added\": {\"name\": \"product image\", \"object\": \"Shark S900 Dual Special Edition - Matt Black\"}}, {\"added\": {\"name\": \"product image\", \"object\": \"Shark S900 Dual Special Edition - Matt Black\"}}]', 9, 1),
(15, '2022-12-20 12:18:52.839898', '5', 'Accessories', 2, '[{\"changed\": {\"fields\": [\"Category Name\"]}}]', 8, 1),
(16, '2022-12-20 12:19:22.022352', '6', 'Parts', 2, '[{\"changed\": {\"fields\": [\"Category Name\"]}}]', 8, 1),
(17, '2022-12-20 12:19:42.648427', '9', 'Gift Ideas', 3, '', 8, 1),
(18, '2022-12-20 12:19:42.650788', '8', 'Motocross', 3, '', 8, 1),
(19, '2022-12-20 12:19:42.655782', '7', 'Electronics', 3, '', 8, 1),
(20, '2022-12-21 06:08:08.940538', '6', 'Parts', 3, '', 8, 1),
(21, '2022-12-21 06:44:37.275873', '8', 'Jackets', 1, '[{\"added\": {}}]', 12, 1),
(22, '2022-12-21 06:44:49.641399', '9', 'Jeans', 1, '[{\"added\": {}}]', 12, 1),
(23, '2022-12-21 06:44:57.600395', '10', 'Suits', 1, '[{\"added\": {}}]', 12, 1),
(24, '2022-12-21 06:45:36.009399', '11', 'Leather Motorcycle Gloves', 1, '[{\"added\": {}}]', 12, 1),
(25, '2022-12-21 06:45:47.385402', '12', 'Waterproof Gloves', 1, '[{\"added\": {}}]', 12, 1),
(26, '2022-12-21 06:45:56.115400', '13', 'Thermal Motorcycle Gloves', 1, '[{\"added\": {}}]', 12, 1),
(27, '2022-12-21 06:46:06.635399', '14', 'Ladies Motorcycle Gloves', 1, '[{\"added\": {}}]', 12, 1),
(28, '2022-12-21 06:46:15.333399', '15', 'Heated Motorcycle Gloves', 1, '[{\"added\": {}}]', 12, 1),
(29, '2022-12-21 06:46:30.216401', '16', 'Heated Motorcycle Gloves', 1, '[{\"added\": {}}]', 12, 1),
(30, '2022-12-21 06:46:49.914446', '17', 'Road / Race Boots', 1, '[{\"added\": {}}]', 12, 1),
(31, '2022-12-21 06:46:59.290447', '18', 'Waterproof Boots', 1, '[{\"added\": {}}]', 12, 1),
(32, '2022-12-21 06:47:12.384164', '19', 'Ladies Motorcycle Boots', 1, '[{\"added\": {}}]', 12, 1),
(33, '2022-12-21 06:47:27.013160', '20', 'Enduro / Off Road Boots', 1, '[{\"added\": {}}]', 12, 1),
(34, '2022-12-21 06:47:37.940161', '21', 'Urban & Casual Look Boots', 1, '[{\"added\": {}}]', 12, 1),
(35, '2022-12-21 06:47:46.130160', '22', 'Gore-Tex Motorcycle Boots', 1, '[{\"added\": {}}]', 12, 1),
(36, '2022-12-21 06:47:57.595103', '23', 'Touring Boots', 1, '[{\"added\": {}}]', 12, 1),
(37, '2022-12-21 06:48:20.120590', '24', 'Motorcycle Rucksacks', 1, '[{\"added\": {}}]', 12, 1),
(38, '2022-12-21 06:48:30.480592', '25', 'Motorcycle Panniers', 1, '[{\"added\": {}}]', 12, 1),
(39, '2022-12-21 06:48:39.777590', '26', 'Motorcycle Tank Bags', 1, '[{\"added\": {}}]', 12, 1),
(40, '2022-12-21 06:48:49.168586', '27', 'Motorcycle Tail Bags', 1, '[{\"added\": {}}]', 12, 1),
(41, '2022-12-21 06:50:24.635141', '3', 'Alpinestars', 1, '[{\"added\": {}}]', 7, 1),
(42, '2022-12-21 06:50:46.988140', '4', 'Bull-it', 1, '[{\"added\": {}}]', 7, 1),
(43, '2022-12-21 06:51:06.283140', '5', 'Dainese', 1, '[{\"added\": {}}]', 7, 1),
(44, '2022-12-21 06:51:21.510144', '6', 'DXR', 1, '[{\"added\": {}}]', 7, 1),
(45, '2022-12-21 06:51:41.951144', '7', 'Furygan', 1, '[{\"added\": {}}]', 7, 1),
(46, '2022-12-21 06:52:10.252221', '8', 'Bell', 1, '[{\"added\": {}}]', 7, 1),
(47, '2022-12-21 06:52:28.716432', '9', 'LS2', 1, '[{\"added\": {}}]', 7, 1),
(48, '2022-12-21 06:52:42.597220', '10', 'Klim', 1, '[{\"added\": {}}]', 7, 1),
(49, '2022-12-21 06:52:53.251287', '11', 'HJC', 1, '[{\"added\": {}}]', 7, 1),
(50, '2022-12-21 06:53:02.739287', '12', 'Icon', 1, '[{\"added\": {}}]', 7, 1),
(51, '2022-12-21 06:53:13.827317', '13', 'X-Lite', 1, '[{\"added\": {}}]', 7, 1),
(52, '2022-12-21 06:53:40.628565', '14', 'Kilm', 1, '[{\"added\": {}}]', 7, 1),
(53, '2022-12-21 06:54:00.714561', '15', 'Held', 1, '[{\"added\": {}}]', 7, 1),
(54, '2022-12-21 06:54:15.163576', '16', 'RST', 1, '[{\"added\": {}}]', 7, 1),
(55, '2022-12-21 06:54:27.739629', '17', 'Rukka', 1, '[{\"added\": {}}]', 7, 1),
(56, '2022-12-21 06:54:43.115633', '18', 'Held', 1, '[{\"added\": {}}]', 7, 1),
(57, '2022-12-21 06:58:05.038496', '18', 'Held', 3, '', 7, 1),
(58, '2022-12-21 06:58:05.042496', '17', 'Rukka', 3, '', 7, 1),
(59, '2022-12-21 06:58:05.044497', '16', 'RST', 3, '', 7, 1),
(60, '2022-12-21 06:58:05.046497', '15', 'Held', 3, '', 7, 1),
(61, '2022-12-21 06:58:05.048497', '14', 'Kilm', 3, '', 7, 1),
(62, '2022-12-21 06:58:05.050497', '13', 'X-Lite', 3, '', 7, 1),
(63, '2022-12-21 06:58:05.052496', '12', 'Icon', 3, '', 7, 1),
(64, '2022-12-21 06:58:05.054503', '11', 'HJC', 3, '', 7, 1),
(65, '2022-12-21 06:58:05.057498', '10', 'Klim', 3, '', 7, 1),
(66, '2022-12-21 06:58:05.060497', '9', 'LS2', 3, '', 7, 1),
(67, '2022-12-21 06:58:05.061497', '8', 'Bell', 3, '', 7, 1),
(68, '2022-12-21 06:58:05.064496', '7', 'Furygan', 3, '', 7, 1),
(69, '2022-12-21 06:58:05.066497', '6', 'DXR', 3, '', 7, 1),
(70, '2022-12-21 06:58:05.068497', '5', 'Dainese', 3, '', 7, 1),
(71, '2022-12-21 06:58:05.070496', '4', 'Bull-it', 3, '', 7, 1),
(72, '2022-12-21 06:58:05.072497', '3', 'Alpinestars', 3, '', 7, 1),
(73, '2022-12-21 06:58:05.074498', '2', 'Arai', 3, '', 7, 1),
(74, '2022-12-21 06:58:05.076494', '1', 'AGV', 3, '', 7, 1),
(75, '2022-12-21 06:58:29.054003', '19', 'Icon', 1, '[{\"added\": {}}]', 7, 1),
(76, '2022-12-21 06:58:48.584368', '20', 'Klim', 1, '[{\"added\": {}}]', 7, 1),
(77, '2022-12-21 06:59:07.254369', '21', 'TCX', 1, '[{\"added\": {}}]', 7, 1),
(78, '2022-12-21 07:00:47.406969', '22', 'HJC', 1, '[{\"added\": {}}]', 7, 1),
(79, '2022-12-21 07:01:02.674036', '23', 'LS2', 1, '[{\"added\": {}}]', 7, 1),
(80, '2022-12-21 07:01:13.456276', '24', 'Bell', 1, '[{\"added\": {}}]', 7, 1),
(81, '2022-12-21 07:01:43.662275', '25', 'Thor', 1, '[{\"added\": {}}]', 7, 1),
(82, '2022-12-21 07:02:02.776724', '26', 'X-Lite', 1, '[{\"added\": {}}]', 7, 1),
(83, '2022-12-21 07:02:25.206544', '27', 'O Neal', 1, '[{\"added\": {}}]', 7, 1),
(84, '2022-12-21 07:02:37.977478', '28', 'Sidi', 1, '[{\"added\": {}}]', 7, 1),
(85, '2022-12-21 09:35:12.656875', '2', 'Shark S900 Dual Special Edition - Matt Black', 1, '[{\"added\": {}}, {\"added\": {\"name\": \"product image\", \"object\": \"Shark S900 Dual Special Edition - Matt Black\"}}, {\"added\": {\"name\": \"product image\", \"object\": \"Shark S900 Dual Special Edition - Matt Black\"}}, {\"added\": {\"name\": \"product image\", \"object\": \"Shark S900 Dual Special Edition - Matt Black\"}}, {\"added\": {\"name\": \"product image\", \"object\": \"Shark S900 Dual Special Edition - Matt Black\"}}]', 9, 1),
(86, '2022-12-21 09:37:50.998500', '3', 'Scorpion Exo-1400 Air Carbon - Black', 1, '[{\"added\": {}}, {\"added\": {\"name\": \"product image\", \"object\": \"Scorpion Exo-1400 Air Carbon - Black\"}}, {\"added\": {\"name\": \"product image\", \"object\": \"Scorpion Exo-1400 Air Carbon - Black\"}}, {\"added\": {\"name\": \"product image\", \"object\": \"Scorpion Exo-1400 Air Carbon - Black\"}}, {\"added\": {\"name\": \"product image\", \"object\": \"Scorpion Exo-1400 Air Carbon - Black\"}}]', 9, 1),
(87, '2022-12-21 12:55:49.862312', '3', 'Scorpion Exo-1400 Air Carbon - Black', 2, '[{\"changed\": {\"fields\": [\"Subcategory\"]}}]', 9, 1),
(88, '2022-12-22 05:16:02.892969', '4', 'HJC CS-15 - Plain Matt Black', 1, '[{\"added\": {}}, {\"added\": {\"name\": \"product image\", \"object\": \"HJC CS-15 - Plain Matt Black\"}}, {\"added\": {\"name\": \"product image\", \"object\": \"HJC CS-15 - Plain Matt Black\"}}, {\"added\": {\"name\": \"product image\", \"object\": \"HJC CS-15 - Plain Matt Black\"}}]', 9, 1),
(89, '2022-12-22 05:20:05.696747', '29', 'KGV', 1, '[{\"added\": {}}]', 7, 1),
(90, '2022-12-22 05:21:07.377494', '5', 'AGV K5-S - Mono Matt Black', 1, '[{\"added\": {}}, {\"added\": {\"name\": \"product image\", \"object\": \"AGV K5-S - Mono Matt Black\"}}, {\"added\": {\"name\": \"product image\", \"object\": \"AGV K5-S - Mono Matt Black\"}}, {\"added\": {\"name\": \"product image\", \"object\": \"AGV K5-S - Mono Matt Black\"}}]', 9, 1),
(91, '2022-12-22 05:23:10.961608', '6', 'Nolan N87 N-Com - Classic Metal White', 1, '[{\"added\": {}}, {\"added\": {\"name\": \"product image\", \"object\": \"Nolan N87 N-Com - Classic Metal White\"}}, {\"added\": {\"name\": \"product image\", \"object\": \"Nolan N87 N-Com - Classic Metal White\"}}, {\"added\": {\"name\": \"product image\", \"object\": \"Nolan N87 N-Com - Classic Metal White\"}}]', 9, 1),
(92, '2022-12-22 05:24:33.854940', '7', 'X-Lite X-803 Ultra Carbon - Puro Gloss Black Carbon', 1, '[{\"added\": {}}, {\"added\": {\"name\": \"product image\", \"object\": \"X-Lite X-803 Ultra Carbon - Puro Gloss Black Carbon\"}}, {\"added\": {\"name\": \"product image\", \"object\": \"X-Lite X-803 Ultra Carbon - Puro Gloss Black Carbon\"}}, {\"added\": {\"name\": \"product image\", \"object\": \"X-Lite X-803 Ultra Carbon - Puro Gloss Black Carbon\"}}]', 9, 1),
(93, '2022-12-22 05:25:44.701511', '8', 'Shark Ridill 1.2 - Blank White', 1, '[{\"added\": {}}, {\"added\": {\"name\": \"product image\", \"object\": \"Shark Ridill 1.2 - Blank White\"}}, {\"added\": {\"name\": \"product image\", \"object\": \"Shark Ridill 1.2 - Blank White\"}}, {\"added\": {\"name\": \"product image\", \"object\": \"Shark Ridill 1.2 - Blank White\"}}]', 9, 1),
(94, '2022-12-22 05:26:48.951396', '9', 'Shark Spartan Carbon - Skin Black / Red', 1, '[{\"added\": {}}, {\"added\": {\"name\": \"product image\", \"object\": \"Shark Spartan Carbon - Skin Black / Red\"}}, {\"added\": {\"name\": \"product image\", \"object\": \"Shark Spartan Carbon - Skin Black / Red\"}}, {\"added\": {\"name\": \"product image\", \"object\": \"Shark Spartan Carbon - Skin Black / Red\"}}]', 9, 1),
(95, '2022-12-22 05:28:04.622458', '10', 'AGV K1 - Guy Martin Replica', 1, '[{\"added\": {}}, {\"added\": {\"name\": \"product image\", \"object\": \"AGV K1 - Guy Martin Replica\"}}, {\"added\": {\"name\": \"product image\", \"object\": \"AGV K1 - Guy Martin Replica\"}}, {\"added\": {\"name\": \"product image\", \"object\": \"AGV K1 - Guy Martin Replica\"}}]', 9, 1),
(96, '2022-12-22 05:29:18.999730', '11', 'Shark Ridill 1.2 - Blank Black', 1, '[{\"added\": {}}, {\"added\": {\"name\": \"product image\", \"object\": \"Shark Ridill 1.2 - Blank Black\"}}, {\"added\": {\"name\": \"product image\", \"object\": \"Shark Ridill 1.2 - Blank Black\"}}, {\"added\": {\"name\": \"product image\", \"object\": \"Shark Ridill 1.2 - Blank Black\"}}]', 9, 1),
(97, '2022-12-22 05:30:21.891234', '12', 'Shark Spartan Carbon - Skin Black / Anthracite', 1, '[{\"added\": {}}, {\"added\": {\"name\": \"product image\", \"object\": \"Shark Spartan Carbon - Skin Black / Anthracite\"}}, {\"added\": {\"name\": \"product image\", \"object\": \"Shark Spartan Carbon - Skin Black / Anthracite\"}}, {\"added\": {\"name\": \"product image\", \"object\": \"Shark Spartan Carbon - Skin Black / Anthracite\"}}]', 9, 1),
(98, '2022-12-22 05:32:11.916781', '13', 'Shoei X-Spirit 3 - Matt Black', 1, '[{\"added\": {}}, {\"added\": {\"name\": \"product image\", \"object\": \"Shoei X-Spirit 3 - Matt Black\"}}, {\"added\": {\"name\": \"product image\", \"object\": \"Shoei X-Spirit 3 - Matt Black\"}}, {\"added\": {\"name\": \"product image\", \"object\": \"Shoei X-Spirit 3 - Matt Black\"}}]', 9, 1),
(99, '2022-12-22 06:33:24.914532', '29', 'KGV', 2, '[{\"changed\": {\"fields\": [\"Logo\"]}}]', 7, 1),
(100, '2022-12-22 06:33:32.692356', '28', 'Sidi', 2, '[{\"changed\": {\"fields\": [\"Logo\"]}}]', 7, 1),
(101, '2022-12-22 06:33:40.240846', '27', 'O Neal', 2, '[{\"changed\": {\"fields\": [\"Logo\"]}}]', 7, 1),
(102, '2022-12-22 06:33:48.414665', '26', 'X-Lite', 2, '[{\"changed\": {\"fields\": [\"Logo\"]}}]', 7, 1),
(103, '2022-12-22 06:33:58.739803', '25', 'Thor', 2, '[{\"changed\": {\"fields\": [\"Logo\"]}}]', 7, 1),
(104, '2022-12-22 06:34:23.359860', '24', 'Bell', 2, '[{\"changed\": {\"fields\": [\"Logo\"]}}]', 7, 1),
(105, '2022-12-22 06:34:34.859405', '23', 'LS2', 2, '[{\"changed\": {\"fields\": [\"Logo\"]}}]', 7, 1),
(106, '2022-12-22 06:36:10.602139', '22', 'HJC', 2, '[{\"changed\": {\"fields\": [\"Logo\"]}}]', 7, 1),
(107, '2022-12-22 06:36:20.022939', '19', 'Icon', 2, '[{\"changed\": {\"fields\": [\"Logo\"]}}]', 7, 1),
(108, '2022-12-22 06:36:27.187807', '21', 'TCX', 2, '[{\"changed\": {\"fields\": [\"Logo\"]}}]', 7, 1),
(109, '2022-12-22 06:36:36.535906', '22', 'HJC', 2, '[{\"changed\": {\"fields\": [\"Logo\"]}}]', 7, 1),
(110, '2022-12-22 06:37:23.655880', '20', 'Klim', 2, '[{\"changed\": {\"fields\": [\"Logo\"]}}]', 7, 1),
(111, '2022-12-22 06:47:24.741087', '13', 'Shoei X-Spirit 3 - Matt Black', 2, '[{\"changed\": {\"fields\": [\"Image\"]}}]', 9, 1),
(112, '2022-12-22 06:47:34.812797', '13', 'Shoei X-Spirit 3 - Matt Black', 2, '[{\"changed\": {\"fields\": [\"Image\"]}}]', 9, 1),
(113, '2022-12-26 07:20:41.773296', '13', 'Shoei X-Spirit 3 - Matt Black', 2, '[{\"changed\": {\"fields\": [\"Product variation\"]}}]', 9, 1),
(114, '2022-12-26 07:21:26.598263', '10', 'AGV K1 - Guy Martin Replica', 2, '[{\"changed\": {\"fields\": [\"Product variation\"]}}]', 9, 1),
(115, '2022-12-26 07:22:27.400791', '12', 'Shark Spartan Carbon - Skin Black / Anthracite', 2, '[{\"changed\": {\"fields\": [\"Product variation\"]}}]', 9, 1),
(116, '2022-12-26 07:22:44.506633', '9', 'Shark Spartan Carbon - Skin Black / Red', 2, '[{\"changed\": {\"fields\": [\"Product variation\"]}}]', 9, 1),
(117, '2022-12-26 07:22:52.417517', '8', 'Shark Ridill 1.2 - Blank White', 2, '[{\"changed\": {\"fields\": [\"Product variation\"]}}]', 9, 1),
(118, '2022-12-26 07:23:00.894849', '7', 'X-Lite X-803 Ultra Carbon - Puro Gloss Black Carbon', 2, '[{\"changed\": {\"fields\": [\"Product variation\"]}}]', 9, 1),
(119, '2022-12-26 07:23:07.690620', '7', 'X-Lite X-803 Ultra Carbon - Puro Gloss Black Carbon', 2, '[]', 9, 1),
(120, '2022-12-26 07:23:15.872666', '6', 'Nolan N87 N-Com - Classic Metal White', 2, '[{\"changed\": {\"fields\": [\"Product variation\"]}}]', 9, 1),
(121, '2022-12-26 07:23:26.776803', '5', 'AGV K5-S - Mono Matt Black', 2, '[{\"changed\": {\"fields\": [\"Product variation\"]}}]', 9, 1),
(122, '2022-12-26 07:23:36.288604', '4', 'HJC CS-15 - Plain Matt Black', 2, '[{\"changed\": {\"fields\": [\"Product variation\"]}}]', 9, 1),
(123, '2022-12-26 07:23:44.184663', '3', 'Scorpion Exo-1400 Air Carbon - Black', 2, '[{\"changed\": {\"fields\": [\"Product variation\"]}}]', 9, 1),
(124, '2022-12-26 07:23:52.998545', '2', 'Shark S900 Dual Special Edition - Matt Black', 2, '[{\"changed\": {\"fields\": [\"Product variation\"]}}]', 9, 1),
(125, '2022-12-26 08:43:45.198752', '1', 'Shark S900 Dual Special Edition - Matt Black', 3, '', 15, 1),
(126, '2022-12-26 08:45:08.200031', '2', 'Scorpion Exo-1400 Air Carbon - Black', 3, '', 15, 1),
(127, '2022-12-26 09:24:44.089008', '5', 'Shark S900 Dual Special Edition - Matt Black', 3, '', 15, 1),
(128, '2022-12-26 11:40:44.959296', '28', 'Half face ladies helmet', 1, '[{\"added\": {}}]', 12, 1),
(129, '2022-12-28 10:42:12.603954', '6', 'AGV K5-S - Mono Matt Black', 3, '', 15, 1),
(130, '2023-01-01 05:16:22.747134', '8', 'Scorpion Exo-1400 Air Carbon - Black', 2, '[{\"changed\": {\"fields\": [\"Price\"]}}]', 15, 1),
(131, '2023-01-02 05:04:18.395127', '1', 'admin@gmail.com', 2, '[{\"changed\": {\"fields\": [\"Phone\"]}}]', 6, 1),
(132, '2023-01-02 06:11:46.335685', '2', 'admin', 1, '[{\"added\": {}}]', 16, 1),
(133, '2023-01-02 08:53:14.415827', '1', 'admin', 1, '[{\"added\": {}}]', 7, 1),
(134, '2023-01-02 11:12:47.155198', '2', 'admin', 1, '[{\"added\": {}}]', 7, 1),
(135, '2023-01-02 11:12:58.775886', '1', 'admin', 2, '[{\"changed\": {\"fields\": [\"Current select\"]}}]', 7, 1),
(136, '2023-01-02 11:30:44.418320', '18', 'HJC CS-15 - Plain Matt Black', 2, '[{\"changed\": {\"fields\": [\"Checkoutstatus\"]}}]', 17, 1),
(137, '2023-01-02 11:30:50.680858', '17', 'Shark S900 Dual Special Edition - Matt Black', 2, '[{\"changed\": {\"fields\": [\"Checkoutstatus\"]}}]', 17, 1),
(138, '2023-01-03 05:51:05.198609', '1', 'Order object (1)', 2, '[{\"changed\": {\"fields\": [\"Transaction id\"]}}]', 16, 1),
(139, '2023-01-03 06:16:53.000697', '26', 'Shark S900 Dual Special Edition - Matt Black', 3, '', 17, 1),
(140, '2023-01-03 07:23:57.777878', '5', 'Order object (5)', 2, '[{\"changed\": {\"fields\": [\"Payment status\"]}}]', 16, 1),
(141, '2023-01-03 07:24:26.671833', '5', 'Order object (5)', 2, '[{\"changed\": {\"fields\": [\"Payment status\"]}}]', 16, 1),
(142, '2023-01-03 07:25:41.685746', '6', 'abc@gmail.com', 3, '', 6, 1),
(143, '2023-01-03 07:25:41.688747', '5', 'anik422@gmail.com', 3, '', 6, 1),
(144, '2023-01-03 07:25:41.690747', '4', 'aniks42@gmail.com', 3, '', 6, 1),
(145, '2023-01-03 07:25:41.693749', '2', 'aniks422@gmail.com', 3, '', 6, 1),
(146, '2023-01-03 10:18:41.684624', '9', 'aniks42@gmail.com', 3, '', 6, 1),
(147, '2023-01-03 10:18:41.704556', '8', 'abc@gmail.com', 3, '', 6, 1),
(148, '2023-01-03 10:18:41.704556', '7', 'aniks422@gmail.com', 3, '', 6, 1),
(149, '2023-01-03 10:53:32.443526', '13', 'aniks442@gmail.com', 3, '', 6, 1),
(150, '2023-01-03 10:53:32.449385', '12', 'aniks42225@gmail.com', 3, '', 6, 1),
(151, '2023-01-03 10:53:32.453823', '11', 'aniks4222@gmail.com', 3, '', 6, 1),
(152, '2023-01-03 10:53:32.457751', '10', 'aniks42@gmail.com', 3, '', 6, 1),
(153, '2023-01-05 07:14:08.414360', '14', 'products', 1, '[{\"added\": {}}, {\"added\": {\"name\": \"product image\", \"object\": \"products\"}}, {\"added\": {\"name\": \"product image\", \"object\": \"products\"}}]', 10, 1),
(154, '2023-01-05 07:24:56.663096', '14', 'products', 3, '', 10, 1),
(155, '2023-01-05 07:42:19.842282', '15', 'aniks42582@gmail.com', 3, '', 6, 1),
(156, '2023-01-05 07:42:19.847283', '14', 'durjoy@gmail.com', 3, '', 6, 1),
(157, '2023-01-08 09:58:12.426557', '2', 'admin', 2, '[{\"changed\": {\"fields\": [\"Street address\"]}}]', 7, 1),
(158, '2023-01-08 09:58:59.895109', '2', 'admin', 2, '[{\"changed\": {\"fields\": [\"Street address\"]}}]', 7, 1);

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(1, 'admin', 'logentry'),
(3, 'auth', 'group'),
(2, 'auth', 'permission'),
(7, 'auth_user', 'address'),
(6, 'auth_user', 'user'),
(4, 'contenttypes', 'contenttype'),
(8, 'product', 'brand'),
(17, 'product', 'cart'),
(9, 'product', 'category'),
(16, 'product', 'order'),
(10, 'product', 'product'),
(15, 'product', 'productimage'),
(14, 'product', 'productreward'),
(11, 'product', 'producttag'),
(12, 'product', 'productvariation'),
(13, 'product', 'subcategory'),
(5, 'sessions', 'session');

-- --------------------------------------------------------

--
-- Table structure for table `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` bigint(20) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2023-01-02 08:42:14.115610'),
(2, 'contenttypes', '0002_remove_content_type_name', '2023-01-02 08:42:14.186261'),
(3, 'auth', '0001_initial', '2023-01-02 08:42:14.407229'),
(4, 'auth', '0002_alter_permission_name_max_length', '2023-01-02 08:42:14.468729'),
(5, 'auth', '0003_alter_user_email_max_length', '2023-01-02 08:42:14.478732'),
(6, 'auth', '0004_alter_user_username_opts', '2023-01-02 08:42:14.487730'),
(7, 'auth', '0005_alter_user_last_login_null', '2023-01-02 08:42:14.496731'),
(8, 'auth', '0006_require_contenttypes_0002', '2023-01-02 08:42:14.499730'),
(9, 'auth', '0007_alter_validators_add_error_messages', '2023-01-02 08:42:14.507736'),
(10, 'auth', '0008_alter_user_username_max_length', '2023-01-02 08:42:14.517729'),
(11, 'auth', '0009_alter_user_last_name_max_length', '2023-01-02 08:42:14.528731'),
(12, 'auth', '0010_alter_group_name_max_length', '2023-01-02 08:42:14.549729'),
(13, 'auth', '0011_update_proxy_permissions', '2023-01-02 08:42:14.558728'),
(14, 'auth', '0012_alter_user_first_name_max_length', '2023-01-02 08:42:14.568732'),
(15, 'auth_user', '0001_initial', '2023-01-02 08:42:14.926042'),
(16, 'admin', '0001_initial', '2023-01-02 08:42:15.046037'),
(17, 'admin', '0002_logentry_remove_auto_add', '2023-01-02 08:42:15.060039'),
(18, 'admin', '0003_logentry_add_action_flag_choices', '2023-01-02 08:42:15.072041'),
(19, 'product', '0001_initial', '2023-01-02 08:42:16.054464'),
(20, 'sessions', '0001_initial', '2023-01-02 08:42:16.091465'),
(21, 'product', '0002_order_bank_tran_id_order_card_ref_id_and_more', '2023-01-03 05:33:35.076054'),
(22, 'product', '0003_remove_order_tran_id', '2023-01-03 05:55:42.121995'),
(23, 'product', '0004_alter_order_tran_date', '2023-01-03 06:09:47.005239'),
(24, 'product', '0005_order_payment_method', '2023-01-03 07:05:18.992632'),
(25, 'auth_user', '0002_alter_user_phone', '2023-01-03 10:55:57.592499'),
(26, 'product', '0006_alter_order_payment_status', '2023-01-03 10:55:57.614015');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('1k067ss2utw5z5waxhvf4jx8m4xjgy0y', '.eJxVjMEOwiAQBf-FsyFAYQGP3v0GssAiVQNJaU_Gf9cmPej1zcx7sYDbWsM2aAlzZmcm2el3i5ge1HaQ79hunafe1mWOfFf4QQe_9kzPy-H-HVQc9VsnS0gKlCg2aa38JNTk0E3GRWWQnHQyCm2hJCjSA4BBZQikN14YwMTeH8M7Nrw:1p7aVz:c3iciWo4RvZrvbhXSYQQN5RebAWGz4f8X9d-fqryVNc', '2023-01-03 11:10:31.410581'),
('3vf3a2orxzka9uhpwwrmp1kerd6zy8ju', 'e30:1pBruH:quxtqz2XTnkg7SIxRiIpyrnzsCCA6nw4wpRxGgJBpiQ', '2023-01-15 06:33:17.646156'),
('3ys40m3fznfiamryb7j9nznljsb848di', '.eJxVjMEOwiAQBf-FsyFAYQGP3v0GssAiVQNJaU_Gf9cmPej1zcx7sYDbWsM2aAlzZmcm2el3i5ge1HaQ79hunafe1mWOfFf4QQe_9kzPy-H-HVQc9VsnS0gKlCg2aa38JNTk0E3GRWWQnHQyCm2hJCjSA4BBZQikN14YwMTeH8M7Nrw:1p9heg:F7vTAiXkZZWJEcHt9B0nKWjBplCx0EI61Air-xgEP8Y', '2023-01-09 07:12:14.243624'),
('4qmqlxsd8uoe3cqtbfz1e0ngnik585mu', '.eJxVjMEOwiAQBf-FsyFAYQGP3v0GssAiVQNJaU_Gf9cmPej1zcx7sYDbWsM2aAlzZmcm2el3i5ge1HaQ79hunafe1mWOfFf4QQe_9kzPy-H-HVQc9VsnS0gKlCg2aa38JNTk0E3GRWWQnHQyCm2hJCjSA4BBZQikN14YwMTeH8M7Nrw:1pARrS:wF2bPoTlJlfBrpZ9G4QfsPLabOH5fLTkylLP2OCCD1k', '2023-01-11 08:32:30.638077'),
('aygz4mujjmhicq8230u44s1itdwapq5e', '.eJxVjMEOwiAQBf-FsyFAYQGP3v0GssAiVQNJaU_Gf9cmPej1zcx7sYDbWsM2aAlzZmcm2el3i5ge1HaQ79hunafe1mWOfFf4QQe_9kzPy-H-HVQc9VsnS0gKlCg2aa38JNTk0E3GRWWQnHQyCm2hJCjSA4BBZQikN14YwMTeH8M7Nrw:1p9iph:WMWqyaemyH6Iyl1cCmLa3Gu4-wQw544HmXvsGZc9DIw', '2023-01-09 08:27:41.414099'),
('e06jg9zuimcofsh83a4glc969tvtjqht', 'e30:1pBrv4:6mbJHLHtq9i9lmDNbNbkimD2vEF5EaBCH6klYoid2ac', '2023-01-15 06:34:06.414330'),
('kzx99ppk8jmjk0ajq9zo64ogmkzlzukh', '.eJxVjMEOwiAQBf-FsyFAYQGP3v0GssAiVQNJaU_Gf9cmPej1zcx7sYDbWsM2aAlzZmcm2el3i5ge1HaQ79hunafe1mWOfFf4QQe_9kzPy-H-HVQc9VsnS0gKlCg2aa38JNTk0E3GRWWQnHQyCm2hJCjSA4BBZQikN14YwMTeH8M7Nrw:1pAT2U:Zta5R5GQmX4mWtJ6R1TMLs3vOpYbPs5okqEm-rIzpWs', '2023-01-11 09:47:58.160493'),
('ldixobabfmu7nst951x2n9los6dkl6zv', '.eJxVjMEOwiAQBf-FsyFAYQGP3v0GssAiVQNJaU_Gf9cmPej1zcx7sYDbWsM2aAlzZmcm2el3i5ge1HaQ79hunafe1mWOfFf4QQe_9kzPy-H-HVQc9VsnS0gKlCg2aa38JNTk0E3GRWWQnHQyCm2hJCjSA4BBZQikN14YwMTeH8M7Nrw:1p7aat:Tguk2N-tZGNCr8Tx3axGW6Quzxlw-H6ccV3Pu4bwd7s', '2023-01-03 11:15:35.350462'),
('nz5dd99hewwbnzszxhvtfbn4ccunacsq', '.eJxVjMEOwiAQBf-FsyFAYQGP3v0GssAiVQNJaU_Gf9cmPej1zcx7sYDbWsM2aAlzZmcm2el3i5ge1HaQ79hunafe1mWOfFf4QQe_9kzPy-H-HVQc9VsnS0gKlCg2aa38JNTk0E3GRWWQnHQyCm2hJCjSA4BBZQikN14YwMTeH8M7Nrw:1pDLLR:m7UUUYhI57goLgogOdxRm8u83wGP61KMAXcIerfSpNM', '2023-01-19 08:11:25.812174'),
('qo3xcmpr8ausd29z11d6qp8ftmvg1oro', '.eJxVjMEOwiAQBf-FsyFAYQGP3v0GssAiVQNJaU_Gf9cmPej1zcx7sYDbWsM2aAlzZmcm2el3i5ge1HaQ79hunafe1mWOfFf4QQe_9kzPy-H-HVQc9VsnS0gKlCg2aa38JNTk0E3GRWWQnHQyCm2hJCjSA4BBZQikN14YwMTeH8M7Nrw:1pCe0U:KMFaCZQOk6zO26E5pxulErqlG9D1YhPvWIhHQtlBw0U', '2023-01-17 09:54:54.996408'),
('xr9ibn4ylfh2tptlfnq1m5s10x0w7wom', '.eJxVjMEOwiAQBf-FsyFAYQGP3v0GssAiVQNJaU_Gf9cmPej1zcx7sYDbWsM2aAlzZmcm2el3i5ge1HaQ79hunafe1mWOfFf4QQe_9kzPy-H-HVQc9VsnS0gKlCg2aa38JNTk0E3GRWWQnHQyCm2hJCjSA4BBZQikN14YwMTeH8M7Nrw:1pASVC:rkJX-O12S23x_3gqJ0DC8C7v3lGRGGIVCdgpzrEKHtM', '2023-01-11 09:13:34.759172');

-- --------------------------------------------------------

--
-- Table structure for table `product_brand`
--

CREATE TABLE `product_brand` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `logo` varchar(100) DEFAULT NULL,
  `weburl` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_brand`
--

INSERT INTO `product_brand` (`id`, `name`, `logo`, `weburl`, `created_at`, `updated_at`) VALUES
(19, 'Icon', 'brands/2_Qob1oL0.jpg', 'https://www.agv.com/us/en/', '2022-12-21 06:58:29.050004', '2022-12-22 06:36:20.018848'),
(20, 'Klim', 'brands/6_uA3GdEK.jpg', 'http://www.araihelmet.com/', '2022-12-21 06:58:48.233367', '2022-12-22 06:37:23.651883'),
(21, 'TCX', 'brands/3_wOueqqO.jpg', 'https://www.agv.com/us/en/', '2022-12-21 06:59:07.250370', '2022-12-22 06:36:27.173819'),
(22, 'HJC', 'brands/4_T5n1gkT.jpg', 'https://www.agv.com/us/en/', '2022-12-21 07:00:47.401968', '2022-12-22 06:36:36.531905'),
(23, 'LS2', 'brands/7.jpg', 'http://www.araihelmet.com/', '2022-12-21 07:01:02.668036', '2022-12-22 06:34:34.854403'),
(24, 'Bell', 'brands/6.jpg', 'https://www.agv.com/us/en/', '2022-12-21 07:01:13.450278', '2022-12-22 06:34:23.347860'),
(25, 'Thor', 'brands/5.jpg', 'https://www.agv.com/us/en/', '2022-12-21 07:01:43.658278', '2022-12-22 06:33:58.735802'),
(26, 'X-Lite', 'brands/4.jpg', 'https://www.agv.com/us/en/', '2022-12-21 07:02:02.771726', '2022-12-22 06:33:48.410666'),
(27, 'O Neal', 'brands/3.jpg', 'https://www.agv.com/us/en/', '2022-12-21 07:02:25.202544', '2022-12-22 06:33:40.236843'),
(28, 'Sidi', 'brands/2.jpg', 'http://www.araihelmet.com/', '2022-12-21 07:02:37.973477', '2022-12-22 06:33:32.688356'),
(29, 'KGV', 'brands/1.jpg', 'https://www.agv.com/us/en/', '2022-12-22 05:20:05.636961', '2022-12-22 06:33:24.910535');

-- --------------------------------------------------------

--
-- Table structure for table `product_brand_category`
--

CREATE TABLE `product_brand_category` (
  `id` bigint(20) NOT NULL,
  `brand_id` bigint(20) NOT NULL,
  `category_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_brand_category`
--

INSERT INTO `product_brand_category` (`id`, `brand_id`, `category_id`) VALUES
(1, 19, 1),
(2, 19, 2),
(3, 19, 3),
(4, 19, 4),
(5, 20, 1),
(6, 20, 2),
(7, 20, 3),
(8, 20, 5),
(9, 21, 1),
(10, 21, 4),
(11, 21, 5),
(12, 22, 2),
(13, 22, 3),
(14, 22, 4),
(15, 22, 5),
(16, 23, 1),
(17, 23, 2),
(18, 23, 3),
(19, 23, 4),
(20, 23, 5),
(21, 24, 1),
(22, 24, 2),
(23, 24, 3),
(24, 24, 4),
(25, 24, 5),
(26, 25, 1),
(27, 25, 2),
(28, 25, 4),
(29, 25, 5),
(30, 26, 1),
(31, 26, 2),
(32, 26, 3),
(33, 26, 4),
(34, 26, 5),
(35, 27, 1),
(36, 27, 2),
(37, 28, 1),
(38, 28, 2),
(39, 28, 3),
(40, 28, 4),
(41, 28, 5),
(42, 29, 1),
(43, 29, 2),
(44, 29, 3);

-- --------------------------------------------------------

--
-- Table structure for table `product_cart`
--

CREATE TABLE `product_cart` (
  `id` bigint(20) NOT NULL,
  `checkoutstatus` tinyint(1) NOT NULL,
  `order_id` varchar(100) NOT NULL,
  `quantity` smallint(6) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `product_variant_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_cart`
--

INSERT INTO `product_cart` (`id`, `checkoutstatus`, `order_id`, `quantity`, `price`, `created_at`, `updated_at`, `product_id`, `product_variant_id`, `user_id`) VALUES
(17, 1, '1ZF47FL715', 2, 41598.00, '2023-01-02 05:20:37.704297', '2023-01-02 11:55:00.156022', 2, 2, 1),
(18, 1, '1ZF47FL715', 2, 19732.00, '2023-01-02 10:42:11.199730', '2023-01-02 11:55:00.159023', 4, 3, 1),
(19, 1, '86CRH4EYJ1', 1, 39998.00, '2023-01-02 11:57:30.263372', '2023-01-02 11:58:50.690322', 3, 3, 1),
(20, 1, '86CRH4EYJ1', 1, 56145.00, '2023-01-02 11:57:50.176207', '2023-01-02 11:58:50.693322', 12, 2, 1),
(21, 1, 'EPRI44UIH2', 1, 20799.00, '2023-01-03 05:14:50.197101', '2023-01-03 05:16:17.800809', 2, 1, 1),
(22, 1, 'EPRI44UIH2', 1, 39998.00, '2023-01-03 05:15:03.100161', '2023-01-03 05:16:17.800809', 3, 3, 1),
(23, 1, 'HGWNEP3LQP', 3, 62397.00, '2023-01-03 05:42:31.148609', '2023-01-03 05:42:59.145480', 2, 2, 1),
(24, 1, 'E8CZJSE3GK', 1, 20799.00, '2023-01-03 06:03:47.250852', '2023-01-03 06:04:09.550433', 2, 1, 1),
(25, 1, 'E8CZJSE3GK', 3, 119994.00, '2023-01-03 06:03:54.874819', '2023-01-03 06:04:09.618781', 3, 3, 1),
(27, 1, '94A365EGZZ121701206382', 2, 19732.00, '2023-01-03 06:17:01.209384', '2023-01-03 06:17:32.370765', 4, 1, 1),
(28, 1, '8LL36CXVJP125101397547', 1, 20799.00, '2023-01-03 06:51:01.399541', '2023-01-03 07:21:32.413599', 2, 1, 1),
(29, 1, 'H6RIZ1KVS8132210323455', 1, 20799.00, '2023-01-03 07:22:10.327772', '2023-01-03 07:22:35.773684', 2, 1, 1),
(30, 1, 'H6RIZ1KVS8132210323455', 1, 39998.00, '2023-01-03 07:22:16.564230', '2023-01-03 07:22:35.776612', 3, 3, 1),
(31, 1, 'H6RIZ1KVS8132210323455', 1, 39111.00, '2023-01-03 07:22:21.517389', '2023-01-03 07:22:35.779622', 6, 2, 1),
(32, 1, 'H6RIZ1KVS8132210323455', 1, 15580.00, '2023-01-03 07:22:27.157300', '2023-01-03 07:22:35.783615', 11, NULL, 1),
(37, 1, 'PSIO8GF0MV155554369455', 1, 20799.00, '2023-01-03 09:55:54.376114', '2023-01-03 09:57:43.797391', 2, 1, 1),
(38, 1, 'PSIO8GF0MV155554369455', 1, 9866.00, '2023-01-03 09:56:07.556181', '2023-01-03 09:57:43.801488', 4, 1, 1),
(39, 1, 'SLGY5H57YV155945285028', 1, 20799.00, '2023-01-03 09:59:45.295133', '2023-01-03 10:02:03.176051', 2, 1, 1),
(40, 1, 'VO8AU3F6T3160317229605', 1, 20799.00, '2023-01-03 10:03:17.233677', '2023-01-03 10:03:31.240826', 2, 1, 1),
(41, 1, 'Q1B0ZJWNKL160343885698', 1, 20799.00, '2023-01-03 10:03:43.885698', '2023-01-03 10:03:51.880600', 2, 1, 1),
(42, 1, '7TFMFRT4I4172508843993', 3, 29598.00, '2023-01-03 11:25:08.859172', '2023-01-03 11:26:15.307353', 4, 3, 1),
(43, 1, 'TZTJBKDU8D172731333043', 1, 68921.00, '2023-01-03 11:27:31.341553', '2023-01-03 11:37:21.266490', 13, 1, 1),
(44, 1, 'XM1BP6BTZ1174733527913', 1, 39998.00, '2023-01-03 11:47:33.540469', '2023-01-04 07:09:47.356617', 3, 3, 1),
(45, 1, 'XM1BP6BTZ1174733527913', 1, 56980.00, '2023-01-03 11:47:41.211106', '2023-01-04 07:09:47.361641', 9, 2, 1),
(46, 1, 'XM1BP6BTZ1174733527913', 1, 68921.00, '2023-01-03 11:47:47.591767', '2023-01-04 07:09:47.365586', 13, 1, 1),
(48, 1, 'XM1BP6BTZ1174733527913', 1, 15580.00, '2023-01-03 11:48:04.792438', '2023-01-04 07:09:47.369592', 8, 2, 1),
(49, 1, 'XM1BP6BTZ1174733527913', 1, 39111.00, '2023-01-03 11:51:13.769230', '2023-01-04 07:09:47.372581', 6, 2, 1),
(50, 1, 'XY37F6B6JK131318379473', 1, 20799.00, '2023-01-04 07:13:18.382468', '2023-01-04 07:14:24.296236', 2, 1, 1),
(51, 1, 'XY37F6B6JK131318379473', 1, 57800.00, '2023-01-04 07:13:24.963927', '2023-01-04 07:14:24.301226', 7, 1, 1),
(52, 1, 'GVKUTTWY28131838828385', 8, 166392.00, '2023-01-04 07:18:38.830386', '2023-01-04 07:19:03.489922', 2, 1, 1),
(53, 1, 'HX32EZZWB6132115867303', 4, 159992.00, '2023-01-04 07:21:15.871304', '2023-01-04 07:21:36.716412', 3, 3, 1),
(54, 1, 'WQDE93AA97133656548773', 2, 41598.00, '2023-01-04 07:36:56.550772', '2023-01-05 08:22:07.506253', 2, 1, 1),
(57, 1, '1J0WGKAJNW142544094572', 1, 20799.00, '2023-01-05 08:25:44.098572', '2023-01-05 08:25:52.573571', 2, 1, 1),
(58, 1, 'EVL8AW4RG0142632715090', 1, 39998.00, '2023-01-05 08:26:32.719091', '2023-01-05 08:26:40.088089', 3, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

CREATE TABLE `product_category` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_category`
--

INSERT INTO `product_category` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Helmets', '2022-12-20 08:46:10.228210', '2022-12-20 08:46:10.228210'),
(2, 'Clothing', '2022-12-20 08:46:22.512463', '2022-12-20 08:46:22.512463'),
(3, 'Gloves', '2022-12-20 08:46:31.182101', '2022-12-20 08:46:31.183068'),
(4, 'Boots', '2022-12-20 08:46:52.756113', '2022-12-20 08:46:52.756113'),
(5, 'Accessories', '2022-12-20 08:46:59.429297', '2022-12-20 12:18:52.836899');

-- --------------------------------------------------------

--
-- Table structure for table `product_order`
--

CREATE TABLE `product_order` (
  `id` bigint(20) NOT NULL,
  `order_id` varchar(100) NOT NULL,
  `payment_status` varchar(2) NOT NULL,
  `transaction_id` varchar(100) DEFAULT NULL,
  `total_amount` decimal(10,2) NOT NULL,
  `delivery_status` varchar(2) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `address_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `bank_tran_id` varchar(255) DEFAULT NULL,
  `card_ref_id` varchar(255) DEFAULT NULL,
  `store_amount` decimal(10,2) DEFAULT NULL,
  `tran_date` datetime(6) DEFAULT NULL,
  `val_id` varchar(100) DEFAULT NULL,
  `payment_method` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_order`
--

INSERT INTO `product_order` (`id`, `order_id`, `payment_status`, `transaction_id`, `total_amount`, `delivery_status`, `created_at`, `updated_at`, `address_id`, `user_id`, `bank_tran_id`, `card_ref_id`, `store_amount`, `tran_date`, `val_id`, `payment_method`) VALUES
(1, 'HGWNEP3LQP', 'PR', '20230103114259178148', 64700.90, 'PR', '2023-01-03 05:42:59.178148', '2023-01-03 05:51:05.194608', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'E8CZJSE3GK', 'SU', '20230103120409685345', 145840.76, 'PR', '2023-01-03 06:04:09.685345', '2023-01-03 06:10:02.666476', 2, 1, '230103120958sFUK56ne4IafMYq', 'dc1da4f52669828139e81ef5eb0f48a5a99ea054a131e00a562887d455417dd912', 142194.74, '2023-01-03 12:09:58.000000', '2301031209580P3UH50JhzEEkG8', NULL),
(3, '94A365EGZZ121701206382', 'SU', '20230103121732373753', 20542.62, 'PR', '2023-01-03 06:17:32.374731', '2023-01-03 06:17:55.966999', 2, 1, '230103121752SyyTzDs8y15vOoI', 'dc1da4f52669828139e81ef5eb0f48a5a99ea054a131e00a562887d455417dd912', 20029.05, '2023-01-03 12:17:32.000000', '230103121752I4OUcVxcxNswUpR', NULL),
(4, '8LL36CXVJP125101397547', 'PR', '20230103132132417598', 21646.97, 'PR', '2023-01-03 07:21:32.418598', '2023-01-03 07:21:32.418598', 2, 1, NULL, NULL, NULL, NULL, NULL, 'COD'),
(5, 'H6RIZ1KVS8132210323455', 'PR', '20230103132235786613', 120078.69, 'PR', '2023-01-03 07:22:35.787612', '2023-01-03 07:24:26.669818', 2, 1, NULL, NULL, NULL, NULL, NULL, 'COD'),
(7, 'PSIO8GF0MV155554369455', 'PE', '20230103155743805906', 31858.28, 'PR', '2023-01-03 09:57:43.805906', '2023-01-03 09:57:43.805906', 2, 1, NULL, NULL, NULL, NULL, NULL, 'SSL'),
(8, 'SLGY5H57YV155945285028', 'PE', '20230103160203180408', 21646.97, 'PR', '2023-01-03 10:02:03.181407', '2023-01-03 10:02:03.181407', 2, 1, NULL, NULL, NULL, NULL, NULL, 'SSL'),
(9, 'VO8AU3F6T3160317229605', 'PE', '20230103160331245857', 21646.97, 'PR', '2023-01-03 10:03:31.246992', '2023-01-03 10:03:31.246992', 2, 1, NULL, NULL, NULL, NULL, NULL, 'COD'),
(10, 'Q1B0ZJWNKL160343885698', 'PE', '20230103160351886341', 21646.97, 'PR', '2023-01-03 10:03:51.887343', '2023-01-03 10:03:51.887343', 2, 1, NULL, NULL, NULL, NULL, NULL, 'SSL'),
(11, '7TFMFRT4I4172508843993', 'SU', '20230103172615310760', 30753.93, 'PR', '2023-01-03 11:26:15.311762', '2023-01-03 11:26:57.312893', 2, 1, '23010317265302q0eeRa0zLxGJH', 'dc1da4f52669828139e81ef5eb0f48a5a99ea054a131e00a562887d455417dd917', 29985.08, '2023-01-03 17:26:15.000000', '23010317265304c6mYAIgqQyHMn', 'SSL'),
(12, 'TZTJBKDU8D172731333043', 'SU', '20230103173721270955', 70764.03, 'PR', '2023-01-03 11:37:21.271949', '2023-01-03 11:37:47.762556', 2, 1, '2301031737440HndeOYYBaJsV1h', 'dc1da4f52669828139e81ef5eb0f48a5a99ea054a131e00a562887d455417dd917', 68994.93, '2023-01-03 17:37:21.000000', '230103173744CffxH82Ep3drP1V', 'SSL'),
(13, 'XM1BP6BTZ1174733527913', 'SU', '20230104130515665242', 229728.11, 'PR', '2023-01-04 07:05:15.668236', '2023-01-04 07:09:47.337355', 2, 1, '2301041309480Cmocz59idDC0cJ', 'dc1da4f52669828139e81ef5eb0f48a5a99ea054a131e00a562887d455417dd913', 223984.91, '2023-01-04 13:09:39.000000', '230104130948r2KBcGLQAjSKFVF', NULL),
(14, 'XY37F6B6JK131318379473', 'SU', '20230104131343670750', 81180.97, 'PR', '2023-01-04 07:13:43.672749', '2023-01-04 07:17:58.166097', 2, 1, '230104131425aScRiJIdO4hpKbO', 'dc1da4f52669828139e81ef5eb0f48a5a99ea054a131e00a562887d455417dd913', 79151.45, '2023-01-04 13:14:18.000000', '2301041314250qSU4aw2ROtVAth', 'SSL'),
(15, 'GVKUTTWY28131838828385', 'PE', '20230104131856249364', 172335.72, 'PR', '2023-01-04 07:18:56.253378', '2023-01-04 07:19:03.492924', 2, 1, NULL, NULL, NULL, NULL, NULL, 'COD'),
(16, 'HX32EZZWB6132115867303', 'PE', '20230104132130293192', 165711.72, 'PR', '2023-01-04 07:21:30.295185', '2023-01-04 07:21:36.784898', 2, 1, NULL, NULL, NULL, NULL, NULL, 'COD'),
(18, 'WQDE93AA97133656548773', 'SU', '20230105142155356252', 43173.93, 'PR', '2023-01-05 08:21:55.359251', '2023-01-05 08:22:07.437251', 2, 1, '230105142202DkoBfQlLNyQ4Kwc', 'dc1da4f52669828139e81ef5eb0f48a5a99ea054a131e00a562887d455417dd914', 42094.58, '2023-01-05 14:21:55.000000', '2301051422021NonYX6eSWEIgUS', 'SSL'),
(19, '1J0WGKAJNW142544094572', 'PE', '20230105142552567569', 21646.97, 'PR', '2023-01-05 08:25:52.570574', '2023-01-05 08:25:52.578569', 2, 1, NULL, NULL, NULL, NULL, NULL, 'COD'),
(20, 'EVL8AW4RG0142632715090', 'PE', '20230105142640017089', 41517.93, 'PR', '2023-01-05 08:26:40.020098', '2023-01-05 08:26:40.278091', 2, 1, NULL, NULL, NULL, NULL, NULL, 'COD');

-- --------------------------------------------------------

--
-- Table structure for table `product_product`
--

CREATE TABLE `product_product` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `short_description` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `quantity` int(11) NOT NULL,
  `regular_price` decimal(10,2) NOT NULL,
  `offer_price` decimal(10,2) NOT NULL,
  `vat` decimal(4,2) NOT NULL,
  `image` varchar(100) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `brand_id` bigint(20) NOT NULL,
  `product_tag_id` bigint(20) NOT NULL,
  `subcategory_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_product`
--

INSERT INTO `product_product` (`id`, `name`, `short_description`, `description`, `quantity`, `regular_price`, `offer_price`, `vat`, `image`, `created_at`, `updated_at`, `brand_id`, `product_tag_id`, `subcategory_id`) VALUES
(2, 'Shark S900 Dual Special Edition - Matt Black', 'Full face helmet with Pinlock visor insert included and drop down sun visor', 'Shark S900 Dual Special Edition - Matt Black\r\nShell made from injected thermoplastic resin\r\nMulti-element internal shock-absorber with differentiated density\r\nAirflow directed by integrated ducts\r\nAnti-scratch 2.2 mm visor - Total Vision Anti-fog visor\r\nSlide Lock quick release visor system\r\nRacing upper ventilation system using multi-point air intake and Venturi deflector\r\nStainless steel vent protection spelt grills\r\nFully Removable Interior Lining in microfibre fabric\r\nQuick release chinstrap Microlock\r\nExternal anti-UV lacquer finish\r\nPinlock visor insert included\r\nInternal Anti-scratch Sun visor (UV protected) Fitted\r\nAnti-turbulence system\r\nSpoiler fitted\r\nWeight: 1570g\r\nThe S900 benefits from the new development process thanks to F.E.A. (finite elements analysis). This method is taken from the automotive industry runs with virtual crash tests. Real crash tests come later to complete and reinforce the safety procedure that Shark imperatively leads. The design department put maximum passion and creativity in the S900. The result? A helmet unlike any other!', 50, 32000.00, 20799.00, 3.50, 'product/images_3.jfif', '2022-12-21 09:35:12.284286', '2022-12-26 07:23:52.995394', 19, 2, 1),
(3, 'Scorpion Exo-1400 Air Carbon - Black', 'Carbon full-face helmet with MaxVision and internal sun visor', 'Constructed from carbon incorporating TCT Thermodynamical Composite Technology\r\nAirfit system permitting personalisation of helmet fit through air adjustable cheek pads with additional noise reduction\r\nPinlock MaxVision ready (insert included)\r\nInternal retractable sun visor\r\nHypoallergenic, removable, machine washable KwikWick3 lining\r\nAdjustable, indexed front and rear vents with aero-tuned rear spoiler reduce lift\r\nBreath-deflector for enhanced fog-free performance\r\nChin-cover contributing to noise reduction\r\nDouble D ring closure\r\n3 shell sizes\r\n5 year manufacturer\'s warranty', 50, 48000.00, 39998.00, 3.50, 'product/images_1.jfif', '2022-12-21 09:37:50.928925', '2022-12-26 07:23:44.180664', 20, 1, 3),
(4, 'HJC CS-15 - Plain Matt Black', 'Entry level polycarbonate composite helmet', 'Polycarbonate composite shell\r\nA quick release visor system\r\nFully removable and washable Nylax interior\r\nGlasses groove for reduced temporal pressure\r\nAdvanced air channelling system\r\nA quick release fastening system\r\n\r\nAll helmets are supplied with a clear visor as standard\r\n\r\nThe CS-15 features HJC\'s advanced channelling system to direct air around the skull for maximum cooling and a matching machine-washable removable comfort liner. The latest CAD engineered internals guarantee the legendary HJC comfort fit and a quick-release anti-fog prepared visor provides good visibility regardless of the conditions. a micro-buckle retention strap completes this good-looking, user-friendly helmet.', 50, 14800.00, 9866.00, 3.50, 'product/images_2.jfif', '2022-12-22 05:16:02.877965', '2022-12-26 07:23:36.284604', 20, 2, 2),
(5, 'AGV K5-S - Mono Matt Black', 'Multi-composite sports helmet with pressure-free fit interior and an internal drop-down sun visor', 'Mixed carbon and fibreglass construction\r\nIntegrated internal drop-down sun visor\r\nEnhanced IVS (Integrated Ventilation System)\r\nPressure-free fit interior and cheek pads\r\nFully removable and washable inner liners\r\nMax Vision Pinlock ready visor (insert included)\r\nVisor can be removed without using tools\r\nWind tunnel tested for optimum air flow\r\nIntegrated rear spoiler for added stability\r\nDouble D retention system\r\nWeighs approximately 1350g (+/- 50g)\r\nCompatible with AGV share system\r\nECE 22.05 certified\r\n2 years extra warranty when registering a helmet with AGV within 60 days of purchase\r\nThis premium sports helmet now features a new inner liner construction, designed with high-performance fabrics and with no stitching in sensitive areas, making for an extremely comfortable fit. The lightweight shell is made from a carbon and fibreglass mix, while stability and aerodynamic performance are maximised thanks to an incorporated spoiler. The Integrated Ventilation System (IVS) has vents that are hollowed into the shell, and there\'s also an internal drop-down sun visor, a removable breath deflector and a chin curtain that keeps noise to a minimum.', 50, 49300.00, 39111.00, 5.20, 'product/dd9b1219f289aa8ab2b2f49aec6bffaf.jpg', '2022-12-22 05:21:06.998454', '2022-12-26 07:23:26.772789', 29, 3, 2),
(6, 'Nolan N87 N-Com - Classic Metal White', 'Multi-composite sports helmet with pressure-free fit interior and an internal drop-down sun visor', 'Mixed carbon and fibreglass construction\r\nIntegrated internal drop-down sun visor\r\nEnhanced IVS (Integrated Ventilation System)\r\nPressure-free fit interior and cheek pads\r\nFully removable and washable inner liners\r\nMax Vision Pinlock ready visor (insert included)\r\nVisor can be removed without using tools\r\nWind tunnel tested for optimum air flow\r\nIntegrated rear spoiler for added stability\r\nDouble D retention system\r\nWeighs approximately 1350g (+/- 50g)\r\nCompatible with AGV share system\r\nECE 22.05 certified\r\n2 years extra warranty when registering a helmet with AGV within 60 days of purchase\r\nThis premium sports helmet now features a new inner liner construction, designed with high-performance fabrics and with no stitching in sensitive areas, making for an extremely comfortable fit. The lightweight shell is made from a carbon and fibreglass mix, while stability and aerodynamic performance are maximised thanks to an incorporated spoiler. The Integrated Ventilation System (IVS) has vents that are hollowed into the shell, and there\'s also an internal drop-down sun visor, a removable breath deflector and a chin curtain that keeps noise to a minimum.', 50, 49300.00, 39111.00, 3.60, 'product/R_1.jfif', '2022-12-22 05:23:10.584431', '2022-12-26 07:23:15.868653', 29, 9, 4),
(7, 'X-Lite X-803 Ultra Carbon - Puro Gloss Black Carbon', 'Full face race helmet with wide visor, racing airflow system, adjustable spoiler and comfort lining', 'Full face race helmet with wide visor, racing airflow system, adjustable spoiler and comfort liningFull face race helmet with wide visor, racing airflow system, adjustable spoiler and comfort liningFull face race helmet with wide visor, racing airflow system, adjustable spoiler and comfort liningFull face race helmet with wide visor, racing airflow system, adjustable spoiler and comfort liningFull face race helmet with wide visor, racing airflow system, adjustable spoiler and comfort liningFull face race helmet with wide visor, racing airflow system, adjustable spoiler and comfort liningFull face race helmet with wide visor, racing airflow system, adjustable spoiler and comfort liningFull face race helmet with wide visor, racing airflow system, adjustable spoiler and comfort liningFull face race helmet with wide visor, racing airflow system, adjustable spoiler and comfort liningFull face race helmet with wide visor, racing airflow system, adjustable spoiler and comfort liningFull face race helmet with wide visor, racing airflow system, adjustable spoiler and comfort liningFull face race helmet with wide visor, racing airflow system, adjustable spoiler and comfort liningFull face race helmet with wide visor, racing airflow system, adjustable spoiler and comfort liningFull face race helmet with wide visor, racing airflow system, adjustable spoiler and comfort liningFull face race helmet with wide visor, racing airflow system, adjustable spoiler and comfort liningFull face race helmet with wide visor, racing airflow system, adjustable spoiler and comfort liningFull face race helmet with wide visor, racing airflow system, adjustable spoiler and comfort lining', 50, 72000.00, 57800.00, 3.00, 'product/R_4.jfif', '2022-12-22 05:24:33.495936', '2022-12-26 07:23:07.687655', 21, 8, 4),
(8, 'Shark Ridill 1.2 - Blank White', 'Successor to the S700S featuring an internal sun visor and easy fit system for glasses', 'Successor to the S700S featuring an internal sun visor and easy fit system for glassesSuccessor to the S700S featuring an internal sun visor and easy fit system for glassesSuccessor to the S700S featuring an internal sun visor and easy fit system for glassesSuccessor to the S700S featuring an internal sun visor and easy fit system for glassesSuccessor to the S700S featuring an internal sun visor and easy fit system for glassesSuccessor to the S700S featuring an internal sun visor and easy fit system for glassesSuccessor to the S700S featuring an internal sun visor and easy fit system for glassesSuccessor to the S700S featuring an internal sun visor and easy fit system for glassesSuccessor to the S700S featuring an internal sun visor and easy fit system for glassesSuccessor to the S700S featuring an internal sun visor and easy fit system for glassesSuccessor to the S700S featuring an internal sun visor and easy fit system for glasses', 50, 16400.00, 15580.00, 3.20, 'product/R_2.jfif', '2022-12-22 05:25:44.323886', '2022-12-26 07:22:52.412711', 22, 3, 4),
(9, 'Shark Spartan Carbon - Skin Black / Red', 'Full face helmet using carbon fibre with an internal sun visor and removable washable interior', 'Full face helmet using carbon fibre with an internal sun visor and removable washable interiorFull face helmet using carbon fibre with an internal sun visor and removable washable interiorFull face helmet using carbon fibre with an internal sun visor and removable washable interiorFull face helmet using carbon fibre with an internal sun visor and removable washable interiorFull face helmet using carbon fibre with an internal sun visor and removable washable interiorFull face helmet using carbon fibre with an internal sun visor and removable washable interiorFull face helmet using carbon fibre with an internal sun visor and removable washable interior', 50, 59200.00, 56980.00, 7.00, 'product/R_4_nEeZFLf.jfif', '2022-12-22 05:26:48.587698', '2022-12-26 07:22:44.502634', 20, 3, 5),
(10, 'AGV K1 - Guy Martin Replica', 'Sporty helmet with Double D ring fastening, Dri-Lex washable lining and aerodynamic spoiler design', 'Sporty helmet with Double D ring fastening, Dri-Lex washable lining and aerodynamic spoiler designSporty helmet with Double D ring fastening, Dri-Lex washable lining and aerodynamic spoiler designSporty helmet with Double D ring fastening, Dri-Lex washable lining and aerodynamic spoiler designSporty helmet with Double D ring fastening, Dri-Lex washable lining and aerodynamic spoiler designSporty helmet with Double D ring fastening, Dri-Lex washable lining and aerodynamic spoiler designSporty helmet with Double D ring fastening, Dri-Lex washable lining and aerodynamic spoiler design', 50, 32900.00, 26320.00, 2.00, 'product/OIP_3.jfif', '2022-12-22 05:28:04.271904', '2022-12-26 07:21:26.593269', 29, 4, 2),
(11, 'Shark Ridill 1.2 - Blank Black', 'Successor to the S700S featuring an internal sun visor and easy fit system for glasses', 'Successor to the S700S featuring an internal sun visor and easy fit system for glassesSuccessor to the S700S featuring an internal sun visor and easy fit system for glassesSuccessor to the S700S featuring an internal sun visor and easy fit system for glassesSuccessor to the S700S featuring an internal sun visor and easy fit system for glassesSuccessor to the S700S featuring an internal sun visor and easy fit system for glassesSuccessor to the S700S featuring an internal sun visor and easy fit system for glassesSuccessor to the S700S featuring an internal sun visor and easy fit system for glassesSuccessor to the S700S featuring an internal sun visor and easy fit system for glassesSuccessor to the S700S featuring an internal sun visor and easy fit system for glassesSuccessor to the S700S featuring an internal sun visor and easy fit system for glasses', 50, 16400.00, 15580.00, 6.00, 'product/R_3.jfif', '2022-12-22 05:29:18.619088', '2022-12-22 05:29:18.619088', 21, 9, 5),
(12, 'Shark Spartan Carbon - Skin Black / Anthracite', 'Full face helmet using carbon fibre with an internal sun visor and removable washable interior', 'Full face helmet using carbon fibre with an internal sun visor and removable washable interiorFull face helmet using carbon fibre with an internal sun visor and removable washable interiorFull face helmet using carbon fibre with an internal sun visor and removable washable interiorFull face helmet using carbon fibre with an internal sun visor and removable washable interiorFull face helmet using carbon fibre with an internal sun visor and removable washable interiorFull face helmet using carbon fibre with an internal sun visor and removable washable interiorFull face helmet using carbon fibre with an internal sun visor and removable washable interiorFull face helmet using carbon fibre with an internal sun visor and removable washable interiorFull face helmet using carbon fibre with an internal sun visor and removable washable interior', 50, 59100.00, 56145.00, 4.00, 'product/R_4_gvLtTAP.jfif', '2022-12-22 05:30:21.531885', '2022-12-26 07:22:27.396787', 29, 7, 4),
(13, 'Shoei X-Spirit 3 - Matt Black', 'Flagship race proven helmet with adjustable interior and multiple spoilers', 'Flagship race proven helmet with adjustable interior and multiple spoilersFlagship race proven helmet with adjustable interior and multiple spoilersFlagship race proven helmet with adjustable interior and multiple spoilersFlagship race proven helmet with adjustable interior and multiple spoilersFlagship race proven helmet with adjustable interior and multiple spoilersFlagship race proven helmet with adjustable interior and multiple spoilersFlagship race proven helmet with adjustable interior and multiple spoilersFlagship race proven helmet with adjustable interior and multiple spoilersFlagship race proven helmet with adjustable interior and multiple spoilers', 50, 100000.00, 68921.00, 2.50, 'product/R_5.jfif', '2022-12-22 05:32:11.452131', '2022-12-26 07:20:41.768292', 19, 2, 5);

-- --------------------------------------------------------

--
-- Table structure for table `product_productimage`
--

CREATE TABLE `product_productimage` (
  `id` bigint(20) NOT NULL,
  `images` varchar(100) NOT NULL,
  `product_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_productimage`
--

INSERT INTO `product_productimage` (`id`, `images`, `product_id`) VALUES
(4, 'product/images_2.jfif', 2),
(5, 'product/4tOFJsy9iG.jfif', 2),
(6, 'product/images_1.jfif', 2),
(7, 'product/images_1_y8Kf3z4.jfif', 2),
(8, 'product/4tOFJsy9iG_fnxR1pK.jfif', 3),
(9, 'product/images_2_YLNqE0T.jfif', 3),
(10, 'product/images_3.jfif', 3),
(11, 'product/images_1_ti8Oe5b.jfif', 3),
(12, 'product/4tOFJsy9iG_hnOrpNA.jfif', 4),
(13, 'product/images_3_EGRZHlQ.jfif', 4),
(14, 'images/images_1_prSxkp4.jfif', 4),
(15, 'product/R_5.jfif', 5),
(16, 'product/OIP_3.jfif', 5),
(17, 'product/OIP_2.jfif', 5),
(18, 'product/R_2.jfif', 6),
(19, 'product/OIP_1.jfif', 6),
(20, 'product/R_3.jfif', 6),
(21, 'product/R_3_jpr2c2F.jfif', 7),
(22, 'product/OIP_2_P0NV0oZ.jfif', 7),
(23, 'product/OIP_1_VJsrHyk.jfif', 7),
(24, 'product/OIP_2_w6T2dum.jfif', 8),
(25, 'product/OIP_3_ThrH9nY.jfif', 8),
(26, 'product/OIP_1_e1RTig1.jfif', 8),
(27, 'product/dd9b1219f289aa8ab2b2f49aec6bffaf.jpg', 9),
(28, 'images/R_5_kEMsQnC.jfif', 9),
(29, 'product/R_3_LUA8gIN.jfif', 9),
(30, 'product/R_5_dtN5gwl.jfif', 10),
(31, 'product/dd9b1219f289aa8ab2b2f49aec6bffaf_WcmY36l.jpg', 10),
(32, 'product/OIP_2_HwDkErT.jfif', 10),
(33, 'product/dd9b1219f289aa8ab2b2f49aec6bffaf_SUCqowj.jpg', 11),
(34, 'product/OIP_2_sckpOg4.jfif', 11),
(35, 'product/R_2_CjmiVDS.jfif', 11),
(36, 'product/dd9b1219f289aa8ab2b2f49aec6bffaf_ZnkxvNq.jpg', 12),
(37, 'product/R_2_nMMxKPa.jfif', 12),
(38, 'product/OIP_2_oWqAa0U.jfif', 12),
(39, 'product/OIP_1_oBsxIbC.jfif', 13),
(40, 'product/OIP_3_4hDxeuR.jfif', 13),
(41, 'product/R_3_bHp1dGK.jfif', 13);

-- --------------------------------------------------------

--
-- Table structure for table `product_productreward`
--

CREATE TABLE `product_productreward` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `product_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `product_producttag`
--

CREATE TABLE `product_producttag` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_producttag`
--

INSERT INTO `product_producttag` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'FREE RACE VISOR', '2022-12-20 11:20:02.917479', '2022-12-20 11:20:02.917479'),
(2, '35% OFF', '2022-12-20 11:20:20.428601', '2022-12-20 11:20:20.428601'),
(3, '20% OFF', '2022-12-20 11:20:26.126611', '2022-12-20 11:20:26.126611'),
(4, '25% OFF', '2022-12-20 11:20:32.308428', '2022-12-20 11:20:32.308428'),
(5, 'OVER 10% OFF', '2022-12-20 11:20:43.132264', '2022-12-20 11:20:43.132264'),
(6, 'OVER 20% OFF', '2022-12-20 11:20:48.242693', '2022-12-20 11:20:48.242693'),
(7, 'OVER 25% OFF', '2022-12-20 11:20:55.194516', '2022-12-20 11:20:55.195514'),
(8, 'OVER 35% OFF', '2022-12-20 11:21:01.400430', '2022-12-20 11:21:01.400430'),
(9, 'BEST BUY', '2022-12-20 11:21:09.549510', '2022-12-20 11:21:09.549510');

-- --------------------------------------------------------

--
-- Table structure for table `product_productvariation`
--

CREATE TABLE `product_productvariation` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_productvariation`
--

INSERT INTO `product_productvariation` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'S : 55 - 56', '2022-12-20 11:23:14.464284', '2022-12-20 11:23:14.464284'),
(2, 'M : 57 - 58', '2022-12-20 11:23:23.781376', '2022-12-20 11:23:23.781376'),
(3, 'XL : 61 - 62', '2022-12-20 11:23:30.299109', '2022-12-20 11:23:30.299109'),
(4, 'L : 59 - 60', '2022-12-20 11:23:38.356303', '2022-12-20 11:23:38.356303');

-- --------------------------------------------------------

--
-- Table structure for table `product_product_product_variation`
--

CREATE TABLE `product_product_product_variation` (
  `id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `productvariation_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_product_product_variation`
--

INSERT INTO `product_product_product_variation` (`id`, `product_id`, `productvariation_id`) VALUES
(29, 2, 1),
(30, 2, 2),
(27, 3, 3),
(28, 3, 4),
(25, 4, 1),
(26, 4, 3),
(23, 5, 2),
(24, 5, 4),
(20, 6, 2),
(21, 6, 3),
(22, 6, 4),
(17, 7, 1),
(18, 7, 2),
(19, 7, 3),
(15, 8, 2),
(16, 8, 3),
(12, 9, 2),
(13, 9, 3),
(14, 9, 4),
(5, 10, 1),
(6, 10, 2),
(7, 10, 3),
(8, 10, 4),
(9, 12, 1),
(10, 12, 2),
(11, 12, 3),
(1, 13, 1),
(2, 13, 2),
(3, 13, 3),
(4, 13, 4);

-- --------------------------------------------------------

--
-- Table structure for table `product_subcategory`
--

CREATE TABLE `product_subcategory` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `category_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_subcategory`
--

INSERT INTO `product_subcategory` (`id`, `name`, `created_at`, `updated_at`, `category_id`) VALUES
(1, 'Browse All Helmets', '2022-12-20 09:07:53.785419', '2022-12-20 09:07:53.785419', 1),
(2, 'Full Face Helmets', '2022-12-20 09:08:01.904299', '2022-12-20 09:08:01.904299', 1),
(3, 'Flip Up Helmets', '2022-12-20 09:08:09.589973', '2022-12-20 09:08:09.589973', 1),
(4, 'Adventure Helmets', '2022-12-20 09:08:17.118065', '2022-12-20 09:08:17.118065', 1),
(5, 'Open Face Helmets', '2022-12-20 09:08:24.163657', '2022-12-20 09:08:24.163657', 1),
(6, 'Motocross Helmets', '2022-12-20 09:08:31.625199', '2022-12-20 09:08:31.625199', 1),
(7, 'Kids Helmets', '2022-12-20 09:08:38.926967', '2022-12-20 09:08:38.926967', 1),
(8, 'Jackets', '2022-12-21 06:44:37.273875', '2022-12-21 06:44:37.273875', 2),
(9, 'Jeans', '2022-12-21 06:44:49.639396', '2022-12-21 06:44:49.639396', 2),
(10, 'Suits', '2022-12-21 06:44:57.599397', '2022-12-21 06:44:57.599397', 2),
(11, 'Leather Motorcycle Gloves', '2022-12-21 06:45:36.007401', '2022-12-21 06:45:36.007401', 3),
(12, 'Waterproof Gloves', '2022-12-21 06:45:47.383398', '2022-12-21 06:45:47.383398', 3),
(13, 'Thermal Motorcycle Gloves', '2022-12-21 06:45:56.113406', '2022-12-21 06:45:56.113406', 3),
(14, 'Ladies Motorcycle Gloves', '2022-12-21 06:46:06.634400', '2022-12-21 06:46:06.634400', 3),
(15, 'Heated Motorcycle Gloves', '2022-12-21 06:46:15.331401', '2022-12-21 06:46:15.331401', 3),
(16, 'Heated Motorcycle Gloves', '2022-12-21 06:46:30.215401', '2022-12-21 06:46:30.215401', 3),
(17, 'Road / Race Boots', '2022-12-21 06:46:49.913447', '2022-12-21 06:46:49.913447', 4),
(18, 'Waterproof Boots', '2022-12-21 06:46:59.288449', '2022-12-21 06:46:59.288449', 4),
(19, 'Ladies Motorcycle Boots', '2022-12-21 06:47:12.383161', '2022-12-21 06:47:12.383161', 4),
(20, 'Enduro / Off Road Boots', '2022-12-21 06:47:27.007161', '2022-12-21 06:47:27.008159', 4),
(21, 'Urban & Casual Look Boots', '2022-12-21 06:47:37.895174', '2022-12-21 06:47:37.895174', 4),
(22, 'Gore-Tex Motorcycle Boots', '2022-12-21 06:47:46.088165', '2022-12-21 06:47:46.088165', 4),
(23, 'Touring Boots', '2022-12-21 06:47:57.512103', '2022-12-21 06:47:57.512103', 4),
(24, 'Motorcycle Rucksacks', '2022-12-21 06:48:20.118587', '2022-12-21 06:48:20.118587', 5),
(25, 'Motorcycle Panniers', '2022-12-21 06:48:30.479588', '2022-12-21 06:48:30.479588', 5),
(26, 'Motorcycle Tank Bags', '2022-12-21 06:48:39.776588', '2022-12-21 06:48:39.776588', 5),
(27, 'Motorcycle Tail Bags', '2022-12-21 06:48:49.167587', '2022-12-21 06:48:49.167587', 5),
(28, 'Half face ladies helmet', '2022-12-26 11:40:44.958216', '2022-12-26 11:40:44.958216', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indexes for table `auth_user_address`
--
ALTER TABLE `auth_user_address`
  ADD PRIMARY KEY (`id`),
  ADD KEY `auth_user_address_user_id_f66fb95b_fk_auth_user_user_id` (`user_id`);

--
-- Indexes for table `auth_user_user`
--
ALTER TABLE `auth_user_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `auth_user_user_phone_f7d0eda7_uniq` (`phone`);

--
-- Indexes for table `auth_user_user_groups`
--
ALTER TABLE `auth_user_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_groups_user_id_group_id_fca56026_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_user_groups_group_id_f9a20b05_fk_auth_group_id` (`group_id`);

--
-- Indexes for table `auth_user_user_user_permissions`
--
ALTER TABLE `auth_user_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_user_perm_user_id_permission_id_14b3010f_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_user__permission_id_2900c446_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_auth_user_user_id` (`user_id`);

--
-- Indexes for table `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indexes for table `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Indexes for table `product_brand`
--
ALTER TABLE `product_brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_brand_category`
--
ALTER TABLE `product_brand_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_brand_category_brand_id_category_id_154d5b5c_uniq` (`brand_id`,`category_id`),
  ADD KEY `product_brand_catego_category_id_1855c8b2_fk_product_c` (`category_id`);

--
-- Indexes for table `product_cart`
--
ALTER TABLE `product_cart`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_cart_product_id_ab32bd2e_fk_product_product_id` (`product_id`),
  ADD KEY `product_cart_product_variant_id_3a336755_fk_product_p` (`product_variant_id`),
  ADD KEY `product_cart_user_id_4b8441ee_fk_auth_user_user_id` (`user_id`);

--
-- Indexes for table `product_category`
--
ALTER TABLE `product_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `product_order`
--
ALTER TABLE `product_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_order_address_id_b3600e28_fk_auth_user_address_id` (`address_id`),
  ADD KEY `product_order_user_id_01a7f9c6_fk_auth_user_user_id` (`user_id`);

--
-- Indexes for table `product_product`
--
ALTER TABLE `product_product`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `product_product_product_tag_id_56882b4e_fk_product_producttag_id` (`product_tag_id`),
  ADD KEY `product_product_subcategory_id_b98525d0_fk_product_s` (`subcategory_id`),
  ADD KEY `product_product_brand_id_fcf1b3f3_fk_product_brand_id` (`brand_id`);

--
-- Indexes for table `product_productimage`
--
ALTER TABLE `product_productimage`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_productimage_product_id_544084bb_fk_product_product_id` (`product_id`);

--
-- Indexes for table `product_productreward`
--
ALTER TABLE `product_productreward`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_id` (`product_id`);

--
-- Indexes for table `product_producttag`
--
ALTER TABLE `product_producttag`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `product_productvariation`
--
ALTER TABLE `product_productvariation`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `product_product_product_variation`
--
ALTER TABLE `product_product_product_variation`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_product_product__product_id_productvariat_62833302_uniq` (`product_id`,`productvariation_id`),
  ADD KEY `product_product_prod_productvariation_id_36980873_fk_product_p` (`productvariation_id`);

--
-- Indexes for table `product_subcategory`
--
ALTER TABLE `product_subcategory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_subcategory_category_id_74573633_fk_product_category_id` (`category_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `auth_user_address`
--
ALTER TABLE `auth_user_address`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `auth_user_user`
--
ALTER TABLE `auth_user_user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `auth_user_user_groups`
--
ALTER TABLE `auth_user_user_groups`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_user_user_user_permissions`
--
ALTER TABLE `auth_user_user_user_permissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=159;

--
-- AUTO_INCREMENT for table `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `product_brand`
--
ALTER TABLE `product_brand`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `product_brand_category`
--
ALTER TABLE `product_brand_category`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `product_cart`
--
ALTER TABLE `product_cart`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `product_category`
--
ALTER TABLE `product_category`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `product_order`
--
ALTER TABLE `product_order`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `product_product`
--
ALTER TABLE `product_product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `product_productimage`
--
ALTER TABLE `product_productimage`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `product_productreward`
--
ALTER TABLE `product_productreward`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_producttag`
--
ALTER TABLE `product_producttag`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `product_productvariation`
--
ALTER TABLE `product_productvariation`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `product_product_product_variation`
--
ALTER TABLE `product_product_product_variation`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `product_subcategory`
--
ALTER TABLE `product_subcategory`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Constraints for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `auth_user_address`
--
ALTER TABLE `auth_user_address`
  ADD CONSTRAINT `auth_user_address_user_id_f66fb95b_fk_auth_user_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user_user` (`id`);

--
-- Constraints for table `auth_user_user_groups`
--
ALTER TABLE `auth_user_user_groups`
  ADD CONSTRAINT `auth_user_user_groups_group_id_f9a20b05_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_user_groups_user_id_9fd2c989_fk_auth_user_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user_user` (`id`);

--
-- Constraints for table `auth_user_user_user_permissions`
--
ALTER TABLE `auth_user_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_user__permission_id_2900c446_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_user_user_user__user_id_6afc8fee_fk_auth_user` FOREIGN KEY (`user_id`) REFERENCES `auth_user_user` (`id`);

--
-- Constraints for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user_user` (`id`);

--
-- Constraints for table `product_brand_category`
--
ALTER TABLE `product_brand_category`
  ADD CONSTRAINT `product_brand_catego_category_id_1855c8b2_fk_product_c` FOREIGN KEY (`category_id`) REFERENCES `product_category` (`id`),
  ADD CONSTRAINT `product_brand_category_brand_id_fdb1d226_fk_product_brand_id` FOREIGN KEY (`brand_id`) REFERENCES `product_brand` (`id`);

--
-- Constraints for table `product_cart`
--
ALTER TABLE `product_cart`
  ADD CONSTRAINT `product_cart_product_id_ab32bd2e_fk_product_product_id` FOREIGN KEY (`product_id`) REFERENCES `product_product` (`id`),
  ADD CONSTRAINT `product_cart_product_variant_id_3a336755_fk_product_p` FOREIGN KEY (`product_variant_id`) REFERENCES `product_productvariation` (`id`),
  ADD CONSTRAINT `product_cart_user_id_4b8441ee_fk_auth_user_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user_user` (`id`);

--
-- Constraints for table `product_order`
--
ALTER TABLE `product_order`
  ADD CONSTRAINT `product_order_address_id_b3600e28_fk_auth_user_address_id` FOREIGN KEY (`address_id`) REFERENCES `auth_user_address` (`id`),
  ADD CONSTRAINT `product_order_user_id_01a7f9c6_fk_auth_user_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user_user` (`id`);

--
-- Constraints for table `product_product`
--
ALTER TABLE `product_product`
  ADD CONSTRAINT `product_product_brand_id_fcf1b3f3_fk_product_brand_id` FOREIGN KEY (`brand_id`) REFERENCES `product_brand` (`id`),
  ADD CONSTRAINT `product_product_product_tag_id_56882b4e_fk_product_producttag_id` FOREIGN KEY (`product_tag_id`) REFERENCES `product_producttag` (`id`),
  ADD CONSTRAINT `product_product_subcategory_id_b98525d0_fk_product_s` FOREIGN KEY (`subcategory_id`) REFERENCES `product_subcategory` (`id`);

--
-- Constraints for table `product_productimage`
--
ALTER TABLE `product_productimage`
  ADD CONSTRAINT `product_productimage_product_id_544084bb_fk_product_product_id` FOREIGN KEY (`product_id`) REFERENCES `product_product` (`id`);

--
-- Constraints for table `product_productreward`
--
ALTER TABLE `product_productreward`
  ADD CONSTRAINT `product_productreward_product_id_242d0d9c_fk_product_product_id` FOREIGN KEY (`product_id`) REFERENCES `product_product` (`id`);

--
-- Constraints for table `product_product_product_variation`
--
ALTER TABLE `product_product_product_variation`
  ADD CONSTRAINT `product_product_prod_product_id_2bf37978_fk_product_p` FOREIGN KEY (`product_id`) REFERENCES `product_product` (`id`),
  ADD CONSTRAINT `product_product_prod_productvariation_id_36980873_fk_product_p` FOREIGN KEY (`productvariation_id`) REFERENCES `product_productvariation` (`id`);

--
-- Constraints for table `product_subcategory`
--
ALTER TABLE `product_subcategory`
  ADD CONSTRAINT `product_subcategory_category_id_74573633_fk_product_category_id` FOREIGN KEY (`category_id`) REFERENCES `product_category` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
