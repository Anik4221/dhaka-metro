from django.shortcuts import render
from django.views.generic import TemplateView
from product.models.product_model import Category, Subcategory, Brand, Product
from ..models import ContactMessage


class AboutUs(TemplateView):
    template_name = 'footer/about_us.html'
    context = {}
    context['categorys'] = Category.objects.all().order_by('id')
    context['subcategorys'] = Subcategory.objects.all()
    context['brands'] = Brand.objects.all()
    context['products'] = Product.objects.all()

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, context=self.context)


class ContactUs(TemplateView):
    template_name = 'footer/contact.html'
    context = {}
    context['categorys'] = Category.objects.all().order_by('id')
    context['subcategorys'] = Subcategory.objects.all()
    context['brands'] = Brand.objects.all()
    context['products'] = Product.objects.all()

    def get(self, request, *args, **kwargs):
        self.context['message'] = False
        return render(request, self.template_name, context=self.context)
    def post(self, request):
        if request.method == 'POST':
            name = request.POST.get('con_name')
            email = request.POST.get('con_email')
            subject = request.POST.get('con_subject')
            message = request.POST.get('con_message')
            ContactMessage.objects.create(name=name, email=email, subject=subject, message=message)
            self.context['message'] = 'Successfully send your Message.'
            return render(request, self.template_name, context=self.context)
        return render(request, self.template_name, context=self.context)


class Frequently(TemplateView):
    template_name = 'footer/terms_conditions.html'
    context = {}
    context['categorys'] = Category.objects.all().order_by('id')
    context['subcategorys'] = Subcategory.objects.all()
    context['brands'] = Brand.objects.all()
    context['products'] = Product.objects.all()

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, context=self.context)


class DeliveryInformation(TemplateView):
    template_name = 'footer/delivery_information.html'
    context = {}
    context['categorys'] = Category.objects.all().order_by('id')
    context['subcategorys'] = Subcategory.objects.all()
    context['brands'] = Brand.objects.all()
    context['products'] = Product.objects.all()

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, context=self.context)


class PrivacyPolicy(TemplateView):
    template_name = 'footer/privacy_policy.html'
    context = {}
    context['categorys'] = Category.objects.all().order_by('id')
    context['subcategorys'] = Subcategory.objects.all()
    context['brands'] = Brand.objects.all()
    context['products'] = Product.objects.all()

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, context=self.context)


class Returns(TemplateView):
    template_name = 'footer/returns.html'
    context = {}
    context['categorys'] = Category.objects.all().order_by('id')
    context['subcategorys'] = Subcategory.objects.all()
    context['brands'] = Brand.objects.all()
    context['products'] = Product.objects.all()

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, context=self.context)