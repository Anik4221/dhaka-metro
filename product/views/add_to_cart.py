from ..models.product_model import Product, ProductVariation, Category, Subcategory, Brand
from ..models.cart_models import Cart
from django.views.generic.edit import DeleteView
from django.urls import reverse
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from django.db.models import Sum
from datetime import datetime
import string
import random
# Create your views here.

@login_required
def cartView(request):
    if request.method == 'POST':
        cart = Cart.objects.get(id=request.POST['cart-id'])
        cart.product_variant = ProductVariation.objects.get(id=request.POST['variation']) 
        cart.quantity = request.POST['quan']
        cart.price = float(cart.product.offer_price * int(request.POST['quan']))
        cart.save()
    cart_items = Cart.objects.filter(user=request.user, checkoutstatus=False)
    total_items_price = cart_items.aggregate(Sum("price"))
    vat = 0
    subtotal = 0
    for cart_item in cart_items:
        vat += ((cart_item.price * cart_item.product.vat) / 100) 
        subtotal += cart_item.price 

    total = subtotal + vat + 120
    context = {
       'cart_items':cart_items,
       'vat':float("{:.2f}".format(vat)),
       'subtotal':subtotal,
       'total':float("{:.2f}".format(total)),
       'total_items_price':total_items_price,
       'delivery': 120
    }
    context['categorys'] = Category.objects.all().order_by('id')
    context['subcategorys'] = Subcategory.objects.all()
    context['brands'] = Brand.objects.all()
    context['products'] = Product.objects.all()
    return render(request, 'cart/cart.html', context=context)


def cartCreateView(request, pk):
    product = Product.objects.get(pk=pk)
    if request.user.is_authenticated:
        carts = Cart.objects.filter(user=request.user, checkoutstatus=False)
        cart = carts.first()
        user = request.user
        cart_item_in = False
        if cart:
            for cart in carts:
                try:
                    if cart.product.id == product.id and cart.product_variant.id == int(request.POST['size']):
                        if request.method == 'POST':
                            cart.quantity = cart.quantity + int(request.POST['quantity'])
                            cart.product_variant = ProductVariation.objects.get(id=request.POST['size'])
                            cart.price = (cart.price + (product.offer_price * int(request.POST['quantity'])))
                            cart.save()
                            return HttpResponseRedirect(reverse('product:cart'))
                except:
                    if cart.product == product and cart.product_variant.id == product.product_variation.all().first().id:
                        cart_item_in = cart
            if cart_item_in:
                cart_item_in.quantity = cart_item_in.quantity + 1
                cart_item_in.price = cart_item_in.price + cart_item_in.product.offer_price
                cart_item_in.save()
                return HttpResponseRedirect(reverse('product:cart'))
            order_id = cart.order_id
            
        else:
            now = str(datetime.now())
            p = ''.join(now.split(':'))
            w = ''.join(p.split('-'))
            w = ''.join(w.split('.'))
            tran_id = ''.join(w.split(' '))
            order_id = ''.join(random.choices(string.ascii_uppercase +
                                string.digits, k=4))
            order_id = order_id + tran_id[-5:]
        if request.method == 'POST':
            cook = request.POST['csrfmiddlewaretoken']
            Cart.objects.create(
                user=user, 
                product=product, 
                product_variant= ProductVariation.objects.get(id=request.POST['size']), 
                quantity= int(request.POST['quantity']),
                order_id=order_id, 
                price=(product.offer_price * int(request.POST['quantity']))
            )
        else:
            Cart.objects.create(
                user=user, 
                product=product, 
                product_variant=product.product_variation.all().first(), 
                order_id=order_id, 
                price=product.offer_price)
        return HttpResponseRedirect(reverse('product:cart'))
    else:
        cart = request.session.get('cart', {})
        cart['post'] = False
        if request.method == 'POST':
            cart['post'] = True
            cart['size'] = request.POST['size']
            cart['quantity'] = int(request.POST['quantity'])
        cart['prod_id'] = product.id
        request.session['cart'] = cart
        return HttpResponseRedirect(reverse('auth_user:login'))



class CartItemDelet(DeleteView):
    model = Cart
    success_url = reverse_lazy('product:cart')
    def get(self, request, *args, **kwargs):
        return self.delete(request, *args, **kwargs)



