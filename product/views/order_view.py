from django.shortcuts import render, redirect
from auth_user.models import Address
from ..models.product_model import Category, Subcategory, Brand, Product
from ..models.cart_models import Cart, Order
from sslcommerz_lib import SSLCOMMERZ 
from django.http import HttpResponse 
from django.views.generic import View
from django.db.models import Sum
from datetime import datetime
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required



@login_required
def checkout(request):
    try:
        address = Address.objects.get(user=request.user, current_select=True)
    except:
        return redirect('auth_user:address_create')
    cart_items = Cart.objects.filter(user=request.user, checkoutstatus=False)
    total_items_price = cart_items.aggregate(Sum("price"))
    vat = 0
    subtotal = 0

    for cart_item in cart_items:
        vat += ((cart_item.price * cart_item.product.vat) / 100)
        subtotal += cart_item.price

    total = subtotal + vat + 120
    if request.method == 'POST':
        if address.billing_address == True:
            Address.objects.create(
                user = request.user,
                name = request.POST['full_name'],
                phone = request.POST['phone'],
                street_address = request.POST['address'],
                zip_code = request.POST['zip'],
                city = request.POST['city'],
                current_select = True
            )
            address.current_select = False
            address.save()
        else:
            address.name = request.POST['full_name']
            address.phone = request.POST['phone']
            address.street_address = request.POST['address']
            address.zip_code = request.POST['zip']
            address.city = request.POST['city']
            address.save()
        now = str(datetime.now())
        p = ''.join(now.split(':'))
        w = ''.join(p.split('-'))
        w = ''.join(w.split('.'))
        tran_id = ''.join(w.split(' '))
        if Order.objects.filter(order_id=cart_items[0].order_id).exists():
            order = Order.objects.get(order_id=cart_items[0].order_id)
        else:
            order = Order.objects.create(
                order_id = cart_items[0].order_id,
                address = address,
                user = request.user,
                total_amount = total,
                transaction_id = tran_id,
            )
        if request.POST['payment_method'] == '1':
            settings = { 'store_id': 'trade633426429cde9', 'store_pass': 'trade633426429cde9@ssl', 'issandbox': True }
            sslcz = SSLCOMMERZ(settings)
            post_body = {}
            post_body['total_amount'] = order.total_amount
            post_body['currency'] = "BDT"
            post_body['tran_id'] = order.transaction_id
            post_body['success_url'] = "http://127.0.0.1:8000/success/"
            post_body['fail_url'] = "http://127.0.0.1:8000/fail_url/"
            post_body['cancel_url'] = "http://127.0.0.1:8000/cancel/"
            post_body['emi_option'] = 0
            post_body['cus_name'] = order.address.name
            post_body['cus_email'] = address.user.email
            post_body['cus_phone'] = order.address.phone
            post_body['cus_add1'] = order.address.street_address
            post_body['cus_city'] = order.address.city
            post_body['cus_postcode'] = order.address.zip_code
            post_body['cus_country'] = "Bangladesh"
            post_body['shipping_method'] = "NO"
            post_body['multi_card_name'] = ""
            post_body['num_of_item'] = len(cart_items)
            post_body['product_name'] = order.order_id
            post_body['product_category'] = cart_items[0].product.subcategory.category.name
            post_body['product_profile'] = "general"
            response = sslcz.createSession(post_body) # API response
            return redirect(response["GatewayPageURL"])
        else:
            for cart_item in cart_items:
                cart_item.checkoutstatus = True
                cart_item.save()
            order.payment_method = 'COD'
            order.save()
            context = {
                'address': address,
                "title": "Checkout",
                'cart_items': [],
                'vat': 00.00,
                'subtotal': 00.00,
                'total': 00.00,
                'total_items_price': 00.00,
                'delivery': 120,
                'success': True
            }
            context['categorys'] = Category.objects.all().order_by('id')
            context['subcategorys'] = Subcategory.objects.all()
            context['brands'] = Brand.objects.all()
            context['products'] = Product.objects.all()
            return render(request, 'payment/success.html', context=context)
    context = {
        'address': address,
        "title": "Checkout",
        'cart_items': cart_items,
        'vat': float("{:.2f}".format(vat)),
        'subtotal': subtotal,
        'total': float("{:.2f}".format(total)),
        'total_items_price': total_items_price,
        'delivery': 120
    }
    context['categorys'] = Category.objects.all().order_by('id')
    context['subcategorys'] = Subcategory.objects.all()
    context['brands'] = Brand.objects.all()
    context['products'] = Product.objects.all()
    context['error_msg'] = "Sorry, Your cart is empty."
    if cart_items:
        return render(request, 'cart/checkout.html', context=context)
    else:
        return render(request, 'cart/cart.html', context=context)
       

@method_decorator(csrf_exempt, name='dispatch')
class CheckoutSuccessView(View):
    model = Order
    template_name = 'payment/fail_url.html'

    
    def get(self, request, *args, **kwargs):
        # return render(request, self.template_name,{'transaction':transaction})
        return HttpResponse('nothing to see')

    def post(self, request, *args, **kwargs):
        data = self.request.POST
        settings = { 'store_id': 'trade633426429cde9', 'store_pass': 'trade633426429cde9@ssl', 'issandbox': True }
        sslcz = SSLCOMMERZ(settings)
        post_body = data
        order = Order.objects.get(transaction_id=post_body['tran_id'])
        order.val_id = post_body['val_id']
        order.store_amount = post_body['store_amount']
        order.bank_tran_id = post_body['bank_tran_id']
        order.tran_date = post_body['tran_date']
        order.save()

        if sslcz.hash_validate_ipn(post_body):
            response = sslcz.validationTransactionOrder(post_body['val_id'])
            if response["status"] == "VALID":
                order.payment_status = "SU"
                print("payment", "%"*123)
            order.card_ref_id = response["card_ref_id"]
            order.payment_method = 'SSL'
            order.save()
            cart_items = Cart.objects.filter(user=order.user, checkoutstatus=False)
            for cart_item in cart_items:
                cart_item.checkoutstatus = True
                cart_item.save()
            messages.success(request,'Payment Successfull')
        else:
            messages.error(request,'Hash validation failed')
            print("Hash validation failed")
        context = {}
        context['categorys'] = Category.objects.all().order_by('id')
        context['subcategorys'] = Subcategory.objects.all()
        context['brands'] = Brand.objects.all()
        context['products'] = Product.objects.all()
        if self.request.user.is_authenticated:
            context['cart_items'] = Cart.objects.filter(user=self.request.user, checkoutstatus=False)
            context['total_items_price'] = context['cart_items'].aggregate(Sum("price"))
        return render(request, 'payment/success.html', context=context)

@method_decorator(csrf_exempt, name='dispatch')
class CheckoutFaildView(View):
    template_name = 'payment/fail_url.html'
    context = {}
    context['categorys'] = Category.objects.all().order_by('id')
    context['subcategorys'] = Subcategory.objects.all()
    context['brands'] = Brand.objects.all()
    context['products'] = Product.objects.all()
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, context=self.context)
    def post(self, request, *args, **kwargs):
        data = self.request.POST
        return render(request, self.template_name, context=self.context)
