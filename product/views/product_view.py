from ..models.product_model import Category, Subcategory, Brand, Product, ProductImage
from ..models.cart_models import Cart
from django.views.generic.detail import DetailView
from django.views.generic import TemplateView, ListView
from django.db.models import Sum
from django.db.models import Q
# Create your views here.

class HomeView(TemplateView):
    template_name = 'home/index.html'
    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['categorys'] = Category.objects.all().order_by('id')
        context['subcategorys'] = Subcategory.objects.all()
        context['brands'] = Brand.objects.all()
        context['products'] = Product.objects.all()
        if self.request.user.is_authenticated:
            context['cart_items'] = Cart.objects.filter(user=self.request.user, checkoutstatus=False)
            context['total_items_price'] = context['cart_items'].aggregate(Sum("price"))
        return context



class ProductDetailView(DetailView):
    model = Product
    template_name = 'product/single-product.html'

    def get_context_data(self, **kwargs):
        prod = self.get_object()
        context = super(ProductDetailView, self).get_context_data(**kwargs)
        context['categorys'] = Category.objects.all().order_by('id')
        context['subcategorys'] = Subcategory.objects.all()
        context['brands'] = Brand.objects.all()
        context['products'] = Product.objects.all()
        context['productImages'] = ProductImage.objects.filter(product=prod.id)
        context['product_variation'] = prod.product_variation.all()
        context['related_products'] = Product.objects.filter(subcategory=prod.subcategory).filter(~Q(id = prod.id))
        if self.request.user.is_authenticated:
            context['cart_items'] = Cart.objects.filter(user=self.request.user, checkoutstatus=False)
            context['total_items_price'] = context['cart_items'].aggregate(Sum("price"))
        return context


class ProductListView(ListView):
    paginate_by = 20
    model = Product
    template_name = 'product/shop-grid-fullwidth.html'
   
    def get_queryset(self):
        categories = self.request.GET.get('categories')
        your_search_query = self.request.GET.get('search')
        if categories == '0':
            try:
                return Product.objects.filter(Q(id__icontains=your_search_query) | Q(name__icontains=your_search_query) | Q(short_description__icontains=your_search_query))
            except:
                return []
        else:
            category = Category.objects.get(id=categories)
            subcategorys = Subcategory.objects.filter(category=category)
            products = []
            for subcat in subcategorys:
                product  = Product.objects.filter(subcategory=subcat)
                product.filter(Q(id__icontains=your_search_query) | Q(name__icontains=your_search_query) | Q(short_description__icontains=your_search_query))
                products += product
            return products
    def get_context_data(self, **kwargs):
        context = super(ProductListView, self).get_context_data(**kwargs)
        context['categorys'] = Category.objects.all().order_by('id')
        context['subcategorys'] = Subcategory.objects.all()
        context['brands'] = Brand.objects.all()
        context['products'] = Product.objects.all()
        if self.request.user.is_authenticated:
            context['cart_items'] = Cart.objects.filter(user=self.request.user, checkoutstatus=False)
            context['total_items_price'] = context['cart_items'].aggregate(Sum("price"))
        return context
    


class BrandListView(ListView):
    paginate_by = 2
    model = Product
    template_name = 'product/shop-left-sidebar.html'

    def get_queryset(self):
        return Product.objects.filter(brand=self.kwargs['pk'])
        

    def get_context_data(self, **kwargs):
        context = super(BrandListView, self).get_context_data(**kwargs)
        context['categorys'] = Category.objects.all().order_by('id')
        context['subcategorys'] = Subcategory.objects.all()
        context['brands'] = Brand.objects.all()
        context['products'] = Product.objects.all()
        if self.request.user.is_authenticated:
            context['cart_items'] = Cart.objects.filter(user=self.request.user, checkoutstatus=False)
            context['total_items_price'] = context['cart_items'].aggregate(Sum("price"))
        return context

class CategoryListView(ListView):
    paginate_by = 20
    model = Product
    template_name = 'product/shop-left-sidebar.html'

    def get_queryset(self):
        product_list = []
        subcategorys = Subcategory.objects.filter(category = self.kwargs['pk'])
        for subcategory in subcategorys:
            products = Product.objects.filter(subcategory=subcategory)
            for product in products:
                product_list.append(product)
        return product_list
        

    def get_context_data(self, **kwargs):
        context = super(CategoryListView, self).get_context_data(**kwargs)
        context['categorys'] = Category.objects.all().order_by('id')
        context['subcategorys'] = Subcategory.objects.all()
        context['brands'] = Brand.objects.all()
        context['products'] = Product.objects.all()
        if self.request.user.is_authenticated:
            context['cart_items'] = Cart.objects.filter(user=self.request.user, checkoutstatus=False)
            context['total_items_price'] = context['cart_items'].aggregate(Sum("price"))
        return context



class SubCategoryListView(ListView):
    paginate_by = 20
    model = Product
    template_name = 'product/shop-left-sidebar.html'

    def get_queryset(self):
        return Product.objects.filter(subcategory = self.kwargs['pk'])
        

    def get_context_data(self, **kwargs):
        context = super(SubCategoryListView, self).get_context_data(**kwargs)
        context['categorys'] = Category.objects.all().order_by('id')
        context['subcategorys'] = Subcategory.objects.all()
        context['brands'] = Brand.objects.all()
        context['products'] = Product.objects.all()
        if self.request.user.is_authenticated:
            context['cart_items'] = Cart.objects.filter(user=self.request.user, checkoutstatus=False)
            context['total_items_price'] = context['cart_items'].aggregate(Sum("price"))
        return context
