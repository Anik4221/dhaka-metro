
from django.urls import path
from django.contrib.auth.views import LogoutView
from django.contrib.auth.decorators import login_required
from django.contrib.auth import views as auth_views
from .views import user_views, footer_view

app_name = 'auth_user'

urlpatterns = [
    path('login/', user_views.LoginView.as_view(), name='login'),
    path('logout/', login_required(LogoutView.as_view(next_page='auth_user:login')), name='logout'),
    path('registration/', user_views.registrationView, name='registration'),
    path('profile/', login_required(user_views.ProfileView.as_view()), name='profile'),
    path('address-create/', user_views.address_create, name='address_create'),
    path('order-details/<str:order_id>', user_views.orderDetails, name='order-details'),
    path('generate-pdf/<str:order_id>', user_views.generate_pdf, name='generate_pdf'),


    path("password_reset/", user_views.PasswordResetOverridesView.as_view(template_name='accounts/password_reset.html', 
                                                                form_class=user_views.EmailValidationOnForgotPassword), name="password_reset"),
    path("password_reset/done/<email>/<countSender>/", user_views.PasswordResetDoneOverridesView.as_view(template_name="accounts/password_reset_done.html"), name="password_reset_done"),
    path("reset/<uidb64>/<token>/", auth_views.PasswordResetConfirmView.as_view(template_name="accounts/password_reset_confirm.html"), name="password_reset_confirm"),
    path("reset/done/", auth_views.PasswordResetCompleteView.as_view(template_name="accounts/reset_done.html"), name="password_reset_complete"),


    # footer views url
    path('about-us/', footer_view.AboutUs.as_view(), name='about_us'),
    path('contact-us/', footer_view.ContactUs.as_view(), name='contact_us'),
    path('frequently/', footer_view.Frequently.as_view(), name='frequently'),
    path('delivery-information/', footer_view.DeliveryInformation.as_view(), name='delivery_information'),
    path('privacy-policy/', footer_view.PrivacyPolicy.as_view(), name='privacy_policy'),
    path('returns/', footer_view.Returns.as_view(), name='returns'),
]