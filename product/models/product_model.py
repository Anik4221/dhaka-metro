from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=255, verbose_name='Category Name', unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        return self.name


class Subcategory(models.Model):
    name = models.CharField(max_length=255, verbose_name='Subcategory Name')
    category = models.ForeignKey(Category, related_name='main_Category', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        return self.name

class Brand(models.Model):
    name = models.CharField(max_length=255, verbose_name='Brand Name')
    category = models.ManyToManyField(Category, related_name='Category')
    logo = models.ImageField(upload_to='brands/', null=True, blank=True)
    weburl = models.URLField(max_length=255, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        return self.name


class ProductVariation(models.Model):
    name = models.CharField(max_length=255, verbose_name='ProductVariation Name', unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        return self.name

class ProductTag(models.Model):
    name = models.CharField(max_length=255, verbose_name='Product Tag Name', unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=255, verbose_name='Product Name', unique=True)
    short_description = models.CharField(max_length=255, verbose_name='Short Description')
    description = models.TextField(verbose_name='Description')
    quantity = models.IntegerField(verbose_name='Quantity')
    regular_price = models.DecimalField(max_digits=10, decimal_places=2)
    offer_price = models.DecimalField(max_digits=10, decimal_places=2)
    vat = models.DecimalField(max_digits=4, decimal_places=2, verbose_name='Vat (%)')
    subcategory = models.ForeignKey(Subcategory, on_delete=models.CASCADE)
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE)
    product_variation = models.ManyToManyField(ProductVariation)
    product_tag = models.ForeignKey(ProductTag, on_delete=models.CASCADE)
    image = models.FileField(upload_to='product/',blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        return self.name

class ProductImage(models.Model):
    product = models.ForeignKey(Product, default=None, on_delete=models.CASCADE)
    images = models.FileField(upload_to='product/')
   
    def __str__(self) -> str:
        return self.product.name


class ProductReward(models.Model):
    name = models.CharField(max_length=255)
    product = models.OneToOneField(Product, on_delete=models.CASCADE) 
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def __str__(self) -> str:
        return self.name