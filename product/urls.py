from django.urls import path
from .views import product_view, add_to_cart, order_view, invoicePDF
from django.contrib.auth.decorators import login_required
app_name = 'product'
urlpatterns = [
    path('', product_view.HomeView.as_view(), name='home'),
    path('product/<pk>/', product_view.ProductDetailView.as_view(), name='productDetailView'),
    path('product/', product_view.ProductListView.as_view(), name='productListView'),
    path('brandproduct/<pk>/', product_view.BrandListView.as_view(), name='brandListView'),
    path('categoryproduct/<pk>/', product_view.CategoryListView.as_view(), name='categoryListView'),
    path('subcategoryListView/<pk>/', product_view.SubCategoryListView.as_view(), name='subcategoryListView'),
    path('cart/', add_to_cart.cartView, name='cart'),
    path('checkout/', order_view.checkout, name='checkout'),
    path('success/', order_view.CheckoutSuccessView.as_view(), name='success'),
    path('cancel/', order_view.CheckoutFaildView.as_view(), name='Cancel'),
    path('fail_url/', order_view.CheckoutFaildView.as_view(), name='fail_url'),
    path('checkout/', order_view.checkout, name='checkout'),
    path('addCart/<pk>/', add_to_cart.cartCreateView, name='add_cart'),
    path('delete/<pk>/', login_required(add_to_cart.CartItemDelet.as_view()), name='cart_delete'),
    path('invoice/<str:html>/', invoicePDF.create_invioce_pdf, name='invoice'),
]