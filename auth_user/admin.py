from django.contrib import admin
from .models import User, Address, ContactMessage
# Register your models here.

class UserAdmin(admin.ModelAdmin):
    list_display = ['full_name', 'email', 'phone', 'is_staff', 'is_superuser', 'is_active']
    list_display_links = list_display
admin.site.register(User, UserAdmin)

class AddressAdmin(admin.ModelAdmin):
    list_display = ['name', 'phone', 'street_address', 'zip_code', 'city']
    list_display_links = list_display
admin.site.register(Address, AddressAdmin)

class ContactMessageAdmin(admin.ModelAdmin):
    list_display = ['email','name', 'subject']
    list_display_links = list_display
admin.site.register(ContactMessage, ContactMessageAdmin)