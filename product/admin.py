from django.contrib import admin
from .models.product_model import Category, Subcategory, Brand, ProductVariation, ProductTag, Product, ProductImage, ProductReward
from .models.cart_models import Cart, Order
from django.urls import reverse

# Register your models here.

class ProductImageAdminInline(admin.StackedInline):
    model = ProductImage


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ("id","name")
    list_display_links = list_display

@admin.register(Subcategory)
class SubcategoryAdmin(admin.ModelAdmin):
    list_display = ("id","name", 'category')
    list_display_links = list_display


@admin.register(Brand)
class BrandAdmin(admin.ModelAdmin):
    list_display = ("id","name", "weburl")
    list_display_links = list_display


@admin.register(ProductVariation)
class ProductVariationAdmin(admin.ModelAdmin):
    list_display = ("id","name")
    list_display_links = list_display


@admin.register(ProductTag)
class ProductTagAdmin(admin.ModelAdmin):
    list_display = ("id","name")
    list_display_links = list_display


@admin.register(Product)
class ProductTagAdmin(admin.ModelAdmin):
    list_display = ("id","name", 'short_description', 'subcategory')
    list_display_links = list_display
    search_fields = ["id", "name", 'short_description']
    inlines = [ProductImageAdminInline]
    class Meta:
        model = Product

# @admin.register(ProductImage)
# class ProductImageAdminInline(admin.ModelAdmin):
#     pass


@admin.register(ProductReward)
class ProductRewardAdmin(admin.ModelAdmin):
    list_display = ("id","name", 'product')
    list_display_links = list_display

@admin.register(Cart)
class CartsAdmin(admin.ModelAdmin):
    list_display = ("order_id","user", 'product', 'quantity', 'price')
    list_display_links = list_display




@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ["order_id","payment_status", 'user', 'transaction_id', 'total_amount', "object_link_pdf"]
    list_display_links = ("order_id","payment_status", 'user', 'transaction_id', 'total_amount')
   