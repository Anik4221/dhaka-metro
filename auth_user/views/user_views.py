from django.shortcuts import render, redirect
from django.views.generic import View
from django.contrib.auth import authenticate, login
from django.views.generic import TemplateView
from django.http import HttpResponseRedirect
from django.urls import reverse
from product.models.product_model import Category, Subcategory, Brand, Product, ProductVariation
from product.models.cart_models import Cart, Order
from django.db.models import Sum
from ..models import Address, User
from ..forms import RegisterForm
from django.contrib.auth.decorators import login_required
from django.template.loader import get_template
from django.http import HttpResponse
from datetime import datetime
import string
import random



from io import BytesIO
from django.http import HttpResponse
from django.template.loader import get_template

from xhtml2pdf import pisa


from django.contrib.auth.views import PasswordResetView, PasswordResetDoneView
from django.contrib.auth.forms import PasswordResetForm
from django.core.exceptions import ValidationError



class LoginView(View):
    template_name = 'auth_user/login.html'
    context = {}
    context['categorys'] = Category.objects.all().order_by('id')
    context['subcategorys'] = Subcategory.objects.all()
    context['brands'] = Brand.objects.all()
    context['products'] = Product.objects.all()

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('product:home')
        return render(request, self.template_name, context=self.context)

    def post(self, request):
        if request.method == 'POST':
            user = authenticate(
                email=request.POST['email'], password=request.POST['password'])
            if user is not None:
                login(request, user)
                session_cart = request.session.get('cart', {})
                if session_cart:
                    product = Product.objects.get(id=session_cart['prod_id'])
                    carts = Cart.objects.filter(
                        user=request.user, checkoutstatus=False)
                    cart_item_in = False
                    if carts:
                        cart_first = carts.first()
                        for cart in carts:
                          try:
                            if cart.product == product and cart.product_variant.id == int(session_cart['size']):
                                if session_cart["post"]:
                                    cart.quantity = cart.quantity +  int(session_cart['quantity'])
                                    cart.product_variant = ProductVariation.objects.get(
                                    id=session_cart['size'])
                                    cart.price = (cart.price + (product.offer_price * int(session_cart['quantity'])))
                                    cart.save()
                                    return HttpResponseRedirect(reverse('product:cart'))

                          except Exception as e:
                            if cart.product == product and cart.product_variant.id == product.product_variation.all().first().id:
                              cart_item_in = cart
                        if cart_item_in:
                          cart_item_in.quantity = cart_item_in.quantity + 1
                          cart_item_in.price = cart_item_in.price + cart_item_in.product.offer_price
                          cart_item_in.save()
                          return HttpResponseRedirect(reverse('product:cart'))
                        order_id = cart_first.order_id
                    else:
                        now = str(datetime.now())
                        p = ''.join(now.split(':'))
                        w = ''.join(p.split('-'))
                        w = ''.join(w.split('.'))
                        tran_id = ''.join(w.split(' '))
                        order_id = ''.join(random.choices(string.ascii_uppercase +
                                                          string.digits, k=4))
                        order_id = order_id + tran_id[-5:]
                    if session_cart["post"]:
                        Cart.objects.create(
                            user=user,
                            product=product,
                            product_variant=ProductVariation.objects.get(
                                id=session_cart['size']),
                            order_id=order_id,
                            quantity=session_cart['quantity'],
                            price=(product.offer_price * int(session_cart['quantity'])))
                    else:
                        Cart.objects.create(
                            user=user,
                            product=product,
                            product_variant=product.product_variation.all().first(),
                            order_id=order_id,
                            price=product.offer_price)
                request.session['cart'] = {}
                return HttpResponseRedirect(reverse('product:home'))
            else:
                self.context['message'] = 'Invalid email or password'
        else:
            self.context['message'] = 'Login failed!.'
        return render(request, self.template_name, context=self.context)


def registrationView(request):
    if request.user.is_authenticated:
        return redirect('product:home')
    message = False
    if request.method == 'POST':
        # form = RegisterForm(request.POST)
        if User.object.filter(email=request.POST['email']).exists():
            message = 'Email already exists.'
        elif User.object.filter(phone=request.POST['phone']).exists():
            message = 'Phone already exists.'
        else:
            user = User.objects.create(
                full_name=request.POST['full_name'],
                email=request.POST['email'],
                phone=request.POST['phone'],
                is_active=True,
                )
            user.set_password(request.POST['password1'])
            user.save()
            return redirect('auth_user:login')
            
    context = {
        'message': message
    }
    context['categorys'] = Category.objects.all().order_by('id')
    context['subcategorys'] = Subcategory.objects.all()
    context['brands'] = Brand.objects.all()
    return render(request, 'auth_user/registration.html', context=context)


class ProfileView(TemplateView):
    template_name = 'auth_user/profile.html'

    def get_context_data(self, **kwargs):
        context = super(ProfileView, self).get_context_data(**kwargs)
        context['categorys'] = Category.objects.all().order_by('id')
        context['subcategorys'] = Subcategory.objects.all()
        context['brands'] = Brand.objects.all()
        context['products'] = Product.objects.all()
        context['cart_items'] = Cart.objects.filter(
            user=self.request.user, checkoutstatus=False)
        context['total_items_price'] = context['cart_items'].aggregate(
            Sum("price"))
        context['orders'] = Order.objects.filter(
            user=self.request.user).order_by('-id')
        context['products'] = Product.objects.all()
        try:
            context['address'] = Address.objects.filter(user=self.request.user)
            context['billing_address'] = context['address'].get(
                billing_address=True)
            context['current_address'] = context['address'].get(
                current_select=True)
        except:
            pass

        return context


@login_required
def address_create(request):
    if request.method == 'POST':
        if Address.objects.filter(billing_address=True).exists():
            Address.objects.create(
                user=request.user,
                name=request.POST['full_name'],
                phone=request.POST['phone'],
                street_address=request.POST['address'],
                zip_code=request.POST['zip'],
                city=request.POST['city'],
                current_select=True
            )
        else:
            Address.objects.create(
                user=request.user,
                name=request.POST['full_name'],
                phone=request.POST['phone'],
                street_address=request.POST['address'],
                zip_code=request.POST['zip'],
                city=request.POST['city'],
                billing_address=True,
                current_select=True
            )
        return redirect('product:checkout')
    context = {}
    context['categorys'] = Category.objects.all().order_by('id')
    context['subcategorys'] = Subcategory.objects.all()
    context['brands'] = Brand.objects.all()
    context['products'] = Product.objects.all()
    if request.user.is_authenticated:
        context['cart_items'] = Cart.objects.filter(
            user=request.user, checkoutstatus=False)
        context['total_items_price'] = context['cart_items'].aggregate(
            Sum("price"))
    return render(request, 'auth_user/address.html', context=context)


@login_required
def orderDetails(request, order_id):
    context = {
        'order_id': order_id
    }
    context['categorys'] = Category.objects.all().order_by('id')
    context['subcategorys'] = Subcategory.objects.all()
    context['brands'] = Brand.objects.all()
    context['cart_items'] = Cart.objects.filter(
        user=request.user, checkoutstatus=False)
    context['order_cart_item'] = Cart.objects.filter(
        user=request.user, checkoutstatus=True, order_id=order_id)
    context['total_items_price'] = context['cart_items'].aggregate(
        Sum("price"))
    context['total_order_items_price'] = context['order_cart_item'].aggregate(
        Sum("price"))
    context['orders'] = Order.objects.get(user=request.user, order_id=order_id)
    vat = 0
    subtotal = 0
    for cart_item in context['order_cart_item']:
        vat += ((cart_item.price * cart_item.product.vat) / 100)
        subtotal += cart_item.price

    total = subtotal + vat + 120
    context['subtotal'] = float("{:.2f}".format(subtotal))
    context['total'] = float("{:.2f}".format(total))
    context['vat'] = float("{:.2f}".format(vat))
    try:
        context['address'] = Address.objects.filter(user=request.user)
        context['billing_address'] = context['address'].get(
            billing_address=True)
        context['current_address'] = context['address'].get(
            current_select=True)
    except:
        pass
    return render(request, 'order/orderDetails.html', context=context)



def generate_pdf(request, order_id):
    context = {
        'order_id': order_id
    }
    context['categorys'] = Category.objects.all().order_by('id')
    context['subcategorys'] = Subcategory.objects.all()
    context['brands'] = Brand.objects.all()
    context['cart_items'] = Cart.objects.filter(
        user=request.user, checkoutstatus=False)
    context['order_cart_item'] = Cart.objects.filter(
        user=request.user, checkoutstatus=True, order_id=order_id)
    context['total_items_price'] = context['cart_items'].aggregate(
        Sum("price"))
    context['total_order_items_price'] = context['order_cart_item'].aggregate(
        Sum("price"))
    context['orders'] = Order.objects.get(user=request.user, order_id=order_id)
    vat = 0
    subtotal = 0
    for cart_item in context['order_cart_item']:
        vat += ((cart_item.price * cart_item.product.vat) / 100)
        subtotal += cart_item.price

    total = subtotal + vat + 120
    if context['orders']:
      if context['orders'].store_amount == total:
        payment = total
        due = 0.0
      else:
        payment = context['orders'].store_amount
        due = total - context['orders'].store_amount
    else:
      payment = 0.0
      due = total 
    context['payment'] = float("{:.2f}".format(payment))
    context['due'] = float("{:.2f}".format(due))
    context['subtotal'] = float("{:.2f}".format(subtotal))
    context['total'] = float("{:.2f}".format(total))
    context['vat'] = float("{:.2f}".format(vat))
    template = get_template('order/pdf.html')
    html  = template.render(context)
    result = BytesIO()
    pdf = pisa.pisaDocument(BytesIO(html.encode("utf-8")), result)

    if not pdf.err:
      return HttpResponse(result.getvalue(), content_type='application/pdf')
    return None



class EmailValidationOnForgotPassword(PasswordResetForm):
    def clean_email(self):
        email = self.cleaned_data['email']
        if not User.objects.filter(email__iexact=email, is_active=True).exists():
            raise ValidationError("There is no user registered with the specified email address!")
        return email

class PasswordResetOverridesView(PasswordResetView):
    form_class = EmailValidationOnForgotPassword
    countSender = None
    def get_success_url(self):
        return reverse("auth_user:password_reset_done", kwargs={"email": self.from_email, "countSender": self.countSender})
    def post(self, request, *args: str, **kwargs):
        self.from_email = request.POST["email"]
        self.countSender = request.POST["sendCount"]
        return super().post(request, *args, **kwargs)


class PasswordResetDoneOverridesView(PasswordResetView):
    form_class = EmailValidationOnForgotPassword
    value = None
    extra_context =  {"email": None, 'countSender': None}
    def get(self, request, *args, **kwargs):
        self.extra_context['email'] = kwargs['email']
        self.extra_context['countSender'] = kwargs['countSender']
        return super().get(request, *args, **kwargs)