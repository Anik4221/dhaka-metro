from django.db import models
from .product_model import Product, ProductVariation
from auth_user.models import User 
from auth_user.models import Address
from django.contrib import admin
from django.urls import reverse




class Cart(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    product_variant = models.ForeignKey(ProductVariation, on_delete=models.CASCADE, blank=True, null=True)
    checkoutstatus = models.BooleanField(default=False)
    order_id = models.CharField(max_length=100)
    quantity = models.SmallIntegerField(default=1)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        return self.product.name


class Order(models.Model):
    COD = 'CD'
    CANCELED = "CA"
    PENDING = "PE"
    SUCCESS = "SU"
    FAILED = "FA"
    PROCESSING = "PR"
    DELIVERED = "DE"
    ON_THE_WAY = "OW"
    PAYMENT_STATUS = [
        (SUCCESS, 'Success'),
        (PENDING, 'Pending'),
        (CANCELED, 'Cancelled'),
        (FAILED, 'Failed'),
        (COD, 'Cash on Delivery')
    ]
    DELIVERY_STATUS = [
        (CANCELED, 'Cancelled'),
        (PROCESSING, 'Processing'),
        (DELIVERED, 'Delivered'),
    ]
    SSLCOMMERZ = 'SSL'
    CASH_ON_DELIVERY = 'COD'
    PAYMENT_METHOD = [
        (SSLCOMMERZ, 'SSLCOMMERZ'),
        (CASH_ON_DELIVERY, 'Cash On Delivery')
    ]
    order_id = models.CharField(max_length=100)
    payment_method = models.CharField(max_length=3, choices=PAYMENT_METHOD, null=True, blank=True)
    payment_status = models.CharField(max_length=2, choices=PAYMENT_STATUS, default=PENDING)
    address = models.ForeignKey(Address, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    transaction_id = models.CharField(max_length=100, blank=True, null=True)
    total_amount = models.DecimalField(max_digits=10, decimal_places=2)
    delivery_status = models.CharField(max_length=2, choices=DELIVERY_STATUS, default=PROCESSING)
    tran_date = models.DateTimeField(null=True, blank=True)
    val_id = models.CharField(max_length=100, null=True, blank=True)
    store_amount = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    bank_tran_id = models.CharField(max_length=255, null=True, blank=True)
    card_ref_id = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    
    @admin.display
    def object_link_pdf(self):
        url = reverse('auth_user:generate_pdf', args=(self.order_id,)) 
        return '<a href="%s">PDF Download</a>'% (url)